<?php

namespace App\Http\Controllers\Admin;

use App\MemberPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreMemberPricesRequest;
use App\Http\Requests\Admin\UpdateMemberPricesRequest;
use Yajra\DataTables\DataTables;

class MemberPricesController extends Controller
{

    public function index()
    {
        if (! Gate::allows('member_price_access')) {
            return abort(401);
        }

        $query = MemberPrice::query();
        $template = 'actionsTemplate';
        $gateKey  = 'member_price_';
        $routeKey = 'member_prices';

        // Searching
        if (request('show_deleted') == 1) {
            if (! Gate::allows('member_price_delete')) {
                return abort(401);
            }
            $query->onlyTrashed();
            $template = 'restoreTemplate';
        }

        $member_prices = $query->orderBy('name', 'asc')->get();

        return view('admin.member_prices.index', compact('member_prices', 'template', 'gateKey', 'routeKey'));
    }

    public function create()
    {
        if (! Gate::allows('member_price_create')) {
            return abort(401);
        }

        return view('admin.member_prices.create');
    }

    public function store(StoreMemberPricesRequest $request)
    {
        if (! Gate::allows('member_price_create')) {
            return abort(401);
        }
        $member_price = MemberPrice::create($request->all());

        return redirect()->route('member_prices.index');
    }

    public function edit($id)
    {
        if (! Gate::allows('member_price_edit')) {
            return abort(401);
        }
        $member_price = MemberPrice::findOrFail($id);

        return view('admin.member_prices.edit', compact('member_price'));
    }

    public function update(UpdateMemberPricesRequest $request, $id)
    {
        if (! Gate::allows('member_price_edit')) {
            return abort(401);
        }
        $member_price = MemberPrice::findOrFail($id);
        $member_price->update($request->all());

        return redirect()->route('member_prices.index');
    }

    public function show($id)
    {
        if (! Gate::allows('member_price_view')) {
            return abort(401);
        }
        $member_price = MemberPrice::findOrFail($id);

        return view('admin.member_prices.show', compact('member_price'));
    }

    public function destroy($id)
    {
        if (! Gate::allows('member_price_delete')) {
            return abort(401);
        }
        $member_price = MemberPrice::findOrFail($id);
        $member_price->delete();

        return redirect()->route('member_prices.index');
    }

    public function restore($id)
    {
        if (! Gate::allows('member_price_delete')) {
            return abort(401);
        }
        $member_price = MemberPrice::onlyTrashed()->findOrFail($id);
        $member_price->restore();

        return redirect()->route('member_prices.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows('member_price_delete')) {
            return abort(401);
        }
        $member_price = MemberPrice::onlyTrashed()->findOrFail($id);
        $member_price->forceDelete();

        return redirect()->route('member_prices.index', ['show_deleted' => 1]);
    }
}
