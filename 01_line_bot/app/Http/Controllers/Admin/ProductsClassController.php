<?php

namespace App\Http\Controllers\Admin;

use App\ProductsClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductsClassRequest;
use App\Http\Requests\Admin\UpdateProductsClassRequest;

class ProductsClassController extends Controller
{

    public function index()
    {
        if (! Gate::allows('products_class_access')) {
            return abort(401);
        }

        $query = ProductsClass::query();
        $template = 'actionsTemplate';
        $gateKey  = 'products_class_';
        $routeKey = 'products_class';

        if (request('show_deleted') == 1) {
            if (! Gate::allows('products_class_delete')) {
                return abort(401);
            }
            $query->onlyTrashed();
            $template = 'restoreTemplate';
        }

        $products_class = $query->orderBy('name', 'asc')->get();

        return view('admin.products_class.index', compact('products_class', 'template', 'gateKey', 'routeKey'));
    }

    public function create()
    {
        if (! Gate::allows('products_class_create')) {
            return abort(401);
        }

        $members = \App\Member::get()->pluck('member_id', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        return view('admin.products_class.create', compact('members'));
    }

    public function store(StoreProductsClassRequest $request)
    {
        if (! Gate::allows('products_class_create')) {
            return abort(401);
        }
        $products_class = ProductsClass::create($request->all());

        return redirect()->route('products_class.index');
    }

    public function edit($id)
    {
        if (! Gate::allows('products_class_edit')) {
            return abort(401);
        }
        $products_class = ProductsClass::findOrFail($id);

        return view('admin.products_class.edit', compact('products_class'));
    }

    public function update(UpdateProductsClassRequest $request, $id)
    {
        if (! Gate::allows('products_class_edit')) {
            return abort(401);
        }
        $products_class = ProductsClass::findOrFail($id);
        $products_class->update($request->all());

        return redirect()->route('products_class.index');
    }

    public function show($id)
    {
        if (! Gate::allows('products_class_view')) {
            return abort(401);
        }
        $products_class = ProductsClass::findOrFail($id);

        return view('admin.products_class.show', compact('products_class'));
    }

    public function destroy($id)
    {
        if (! Gate::allows('products_class_delete')) {
            return abort(401);
        }
        $products_class = ProductsClass::findOrFail($id);
        $products_class->delete();

        return redirect()->route('products_class.index');
    }

    public function restore($id)
    {
        if (! Gate::allows('products_class_delete')) {
            return abort(401);
        }
        $products_class = ProductsClass::onlyTrashed()->findOrFail($id);
        $products_class->restore();

        return redirect()->route('products_class.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows('products_class_delete')) {
            return abort(401);
        }
        $products_class = ProductsClass::onlyTrashed()->findOrFail($id);
        $products_class->forceDelete();

        return redirect()->route('products_class.index', ['show_deleted' => 1]);
    }
}
