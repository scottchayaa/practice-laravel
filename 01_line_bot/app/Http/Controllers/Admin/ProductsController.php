<?php

namespace App\Http\Controllers\Admin;

use App\Vendor;
use App\Product;
use App\ProductLog;
use App\ProductsClass;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redis;
use App\Http\Requests\Admin\StoreProductsRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;

class ProductsController extends Controller
{
    private function getWhereConditions()
    {
        $conditions = [];
        if (request('products_class_id')) {
            $conditions[] = [ 'products_class_id', request('products_class_id') ];
        }
        if (request('vendors_id')) {
            $conditions[] = [ 'vendors_id', request('vendors_id') ];
        }
        if (request('model')) {
            $conditions[] = [ 'model', 'like', '%'.request('model').'%' ];
        }
        if (request('size')) {
            $conditions[] = [ 'size', request('size') ];
        }
        if (request('color')) {
            $conditions[] = [ 'color', request('color') ];
        }

        return $conditions;
    }

    public function index()
    {
        if (! Gate::allows('product_access')) {
            return abort(401);
        }

        $query = Product::query();
        $template = 'actionsTemplate';
        $gateKey  = 'product_';
        $routeKey = 'products';

        // Show Trash
        if (request('show_deleted') == 1) {
            if (! Gate::allows('product_delete')) {
                return abort(401);
            }
            $query->onlyTrashed();
            $template = 'restoreTemplate';
        }
        $stocksTemplate = 'stocksTemplate';

        $products = $query->selectRaw('
            products.*,
            products_class.name class_name,
            vendors.name vendor_name
        ')
        ->join('products_class', 'products_class.id', '=', 'products.products_class_id')
        ->join('vendors', 'vendors.id', '=', 'products.vendors_id')
        ->where($this->getWhereConditions())
        ->orderBy('class_name', 'asc')
        ->orderBy('name', 'asc')
        ->get();

        $products_class = ProductsClass::orderBy('name', 'asc')->get();
        $vendors = Vendor::orderBy('name', 'asc')->get();

        return view(
            'admin.products.index',
            compact('products', 'vendors', 'products_class', 'template', 'stocksTemplate', 'gateKey', 'routeKey')
        );
    }

    public function create()
    {
        if (! Gate::allows('product_create')) {
            return abort(401);
        }

        $products_class = ProductsClass::orderBy('name', 'asc')->get();
        $vendors = Vendor::orderBy('name', 'asc')->get();

        return view('admin.products.create', compact('products_class', 'vendors'));
    }

    public function store(StoreProductsRequest $request)
    {
        if (! Gate::allows('product_create')) {
            return abort(401);
        }
        $product = Product::create($request->all());

        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        if (! Gate::allows('product_edit')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);
        $product_logs = ProductLog::where('products_id', $id)->orderBy('created_at', 'desc')->orderBy('id', 'desc')->paginate(config('common.pagesize'));
        $products_class = ProductsClass::orderBy('name', 'asc')->get();
        $vendors = Vendor::orderBy('name', 'asc')->get();

        return view('admin.products.edit', compact('product', 'vendors', 'product_logs', 'products_class'));
    }

    public function update(UpdateProductsRequest $request, $id)
    {
        if (! Gate::allows('product_edit')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);
        $product->update($request->all());

        return redirect()->route('products.index');
    }

    public function show($id)
    {
        if (! Gate::allows('product_view')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);

        return view('admin.products.show', compact('product'));
    }

    public function destroy($id)
    {
        if (! Gate::allows('product_delete')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect()->route('products.index');
    }

    public function restore($id)
    {
        if (! Gate::allows('product_delete')) {
            return abort(401);
        }
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->restore();

        return redirect()->route('products.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows('product_delete')) {
            return abort(401);
        }
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->forceDelete();

        return redirect()->route('products.index', ['show_deleted' => 1]);
    }

    public function Purchase(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $product = Product::find($id);
            $product->stock += $request->quantity;
            $product->save();

            $product_log = ProductLog::create([
                'users_id' => $request->user()->id,
                'products_id' => $id,
                'quantity' => $request->quantity,
                'type' => 2,
                'stock' => $product->stock,
                'target_name' => Vendor::find($product->vendors_id)->name,
                'memo' => $request->memo,
            ]);

            Redis::hset("products:{$id}", "last_purchase_at", $product_log->created_at);
            Redis::hset("products:{$id}", "last_purchase_user", $request->user()->name);
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($product, 200);
    }

    public function Shipment(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $product = Product::find($id);
            $product->stock -= $request->quantity;
            $product->save();

            $product_log = ProductLog::create([
                'users_id' => $request->user()->id,
                'products_id' => $id,
                'quantity' => -$request->quantity,
                'type' => 3,
                'stock' => $product->stock,
                'target_name' => Vendor::find($request->vendors_id)->name,
                'memo' => $request->memo,
            ]);

            Redis::hset("products:{$id}", "last_shipment_at", $product_log->created_at);
            Redis::hset("products:{$id}", "last_shipment_user", $request->user()->name);
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($product, 200);
    }

    public function Inventory(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $product = Product::find($id);
            $diff = $request->quantity - $product->stock; // mark different of stock

            $product->stock = $request->quantity;
            $product->save();

            $product_log = ProductLog::create([
                'users_id' => $request->user()->id,
                'products_id' => $id,
                'quantity' => $diff,
                'type' => 4,
                'stock' => $product->stock,
                'target_name' => null,
                'memo' => $request->memo,
            ]);

            Redis::hset("products:{$id}", "last_inventory_at", $product_log->created_at);
            Redis::hset("products:{$id}", "last_inventory_user", $request->user()->name);
        } catch (\Throwable $th) {
            \DB::rollback();
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }

        \DB::commit();

        return response()->json($product, 200);
    }
}
