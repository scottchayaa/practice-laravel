<?php

namespace App\Http\Controllers\Admin;

use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreVendorsRequest;
use App\Http\Requests\Admin\UpdateVendorsRequest;
use Yajra\DataTables\DataTables;

class VendorsController extends Controller
{

    public function index()
    {
        if (! Gate::allows('vendor_access')) {
            return abort(401);
        }

        $query = Vendor::query();
        $template = 'actionsTemplate';
        $gateKey  = 'vendor_';
        $routeKey = 'vendors';

        // Searching
        if (request('show_deleted') == 1) {
            if (! Gate::allows('vendor_delete')) {
                return abort(401);
            }
            $query->onlyTrashed();
            $template = 'restoreTemplate';
        }

        $vendors = $query->orderBy('name', 'asc')->get();

        return view('admin.vendors.index', compact('vendors', 'template', 'gateKey', 'routeKey'));
    }

    public function create()
    {
        if (! Gate::allows('vendor_create')) {
            return abort(401);
        }

        $members = \App\Member::get()->pluck('member_id', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        return view('admin.vendors.create', compact('members'));
    }

    public function store(StoreVendorsRequest $request)
    {
        if (! Gate::allows('vendor_create')) {
            return abort(401);
        }
        $vendor = Vendor::create($request->all());

        return redirect()->route('vendors.index');
    }

    public function edit($id)
    {
        if (! Gate::allows('vendor_edit')) {
            return abort(401);
        }
        $vendor = Vendor::findOrFail($id);

        return view('admin.vendors.edit', compact('vendor'));
    }

    public function update(UpdateVendorsRequest $request, $id)
    {
        if (! Gate::allows('vendor_edit')) {
            return abort(401);
        }
        $vendor = Vendor::findOrFail($id);
        $vendor->update($request->all());

        return redirect()->route('vendors.index');
    }

    public function destroy($id)
    {
        if (! Gate::allows('vendor_delete')) {
            return abort(401);
        }
        $vendor = Vendor::findOrFail($id);
        $vendor->delete();

        return redirect()->route('vendors.index');
    }

    public function restore($id)
    {
        if (! Gate::allows('vendor_delete')) {
            return abort(401);
        }
        $vendor = Vendor::onlyTrashed()->findOrFail($id);
        $vendor->restore();

        return redirect()->route('vendors.index');
    }

    public function perma_del($id)
    {
        if (! Gate::allows('vendor_delete')) {
            return abort(401);
        }
        $vendor = Vendor::onlyTrashed()->findOrFail($id);
        $vendor->forceDelete();

        return redirect()->route('vendors.index', ['show_deleted' => 1]);
    }
}
