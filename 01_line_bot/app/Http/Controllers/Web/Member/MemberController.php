<?php

namespace App\Http\Controllers\Web\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Services\Line\Login\LineLoginService;
use App\Services\Line\Login\Callbacks\CreateMemberCallback;

class MemberController extends Controller
{
    public function __construct(LineLoginService $lineLoginService)
    {
        $this->lineLoginService = $lineLoginService;
    }

    private function checkLineLogin()
    {
        $request = Request::capture();

        $this->lineLoginService->setScope(['email', 'phone', 'profile', 'openid']);
        $this->lineLoginService->setCallback(CreateMemberCallback::class);
        $this->lineLoginService->enablePrompt(true);
        $this->lineLoginService->setRedirectPageUrl(url($request->path()));
        $this->lineLoginService->redirectIfNotLogin();
    }

    public function index(Request $request)
    {
        $this->checkLineLogin();

        dd($this->lineLoginService->getProfile());
    }
}
