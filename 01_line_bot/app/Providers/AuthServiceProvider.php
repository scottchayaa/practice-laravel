<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Auth gates : User actions
        Gate::define('user_action_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates : Members
        Gate::define('members_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('members_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('members_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('members_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('members_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });


        // Auth gates : System management
        Gate::define('system_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates : Role
        Gate::define('role_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates : Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });
    }
}
