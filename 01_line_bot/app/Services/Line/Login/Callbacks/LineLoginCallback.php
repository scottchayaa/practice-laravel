<?php

namespace App\Services\Line\Login\Callbacks;

/**
 * LineLoginCallback class
 *
 * 用來抽象化所有的 Line Login Callback
 * 皆要處理handle function
 */
abstract class LineLoginCallback
{
    /**
     * Callback Handler
     *
     * @param array $profile
     * @param array $params
     * @return void
     */
    abstract public function handle(array $profile, array $params);
}
