<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTables extends Migration
{
    public function up()
    {
        Schema::create("members", function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('password', 191)->nullable();
            $table->nullableTimestamps();

            $table->unique(["email"], 'email_unique');
        });

        Schema::create("line_members", function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('members_id')->primary();
            $table->string('uid', 45);
            $table->string('displayName', 100)->nullable();
            $table->string('pictureUrl', 255)->nullable();
            $table->tinyInteger('friendFlag')->default(0);
            $table->string('email', 100)->unique()->nullable();
            $table->string('phone', 45)->nullable();
            $table->nullableTimestamps();

            $table->foreign('members_id')
                ->references('id')->on('members')
                ->onDelete('NO ACTION')
                ->onUpdate('NO ACTION');
        });
    }

    public function down()
    {
        Schema::dropIfExists("line_members");
        Schema::dropIfExists("members");
    }
}
