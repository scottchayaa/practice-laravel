
const common = {
    initForm: form => {
        // Hide empty input search fileds
        form.submit(function(){
            $(this).find(":input").filter(function(){ return !this.value; }).attr("disabled", "disabled");
            //callback();
            return true;
        });
    },

    resetForm: form => {
        form.find(':radio, :checkbox').each((index, element)=>{element.checked=false;});
        form.find('textarea, :text, select, input[type="date"], input[type="number"]').val('');
        return false;
    },

    formatNumber: num => {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
}
