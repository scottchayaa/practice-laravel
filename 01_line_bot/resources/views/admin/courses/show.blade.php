@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('courses.index') }}">課程資料</a></li>
        <li class="breadcrumb-item active">{{ $course->name }}</li>
        </ol>
    </nav>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">課程資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">排序</th>
                    <td>
                        <td>{{ $course->sort }}</td>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">課程名稱</th>
                    <td>{{ $course->name }}</td>
                </tr>
                <tr>
                    <th>上課時間</th>
                    <td>{{ $course->created_at }}</td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>{{ $course->memo }}</td>
                </tr>
            </table>
        </div>
    </div>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


