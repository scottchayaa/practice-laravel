@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('member_prices.index') }}">入會費資料</a></li>
        <li class="breadcrumb-item active">編輯</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('member_prices.update', $member_price->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">編輯入會費資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">項目名稱</th>
                    <td>
                        <input type="text" class="form-control" name="name" value="{{ $member_price->name }}">
                    </td>
                </tr>
                <tr>
                    <th>費用</th>
                    <td>
                        <input type="number" class="form-control" name="price" value="{{ $member_price->price }}">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo" value="{{ $member_price->memo }}">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Update</button>
            </p>
        </div>
    </div>
    </form>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


