@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">入會費資料</li>
        </ol>
    </nav>

    @can('member_price_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('member_prices.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: bold' }}">All</a></li> |
            <li><a href="{{ route('member_prices.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: bold' : '' }}">Trash</a></li>
        </ul>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            @can('member_price_create')
            <a href="{{ route('member_prices.create') }}" class="btn btn-default">
                <i class="fa fa-plus"></i> 新增
            </a>
            @endcan
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-md-2">項目名稱</th>
                        <th class="col-md-2">費用</th>
                        <th>備註</th>
                        <th class="col-md-2">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($member_prices as $row)
                    <tr>
                        <td>{{ $row->name }}</td>
                        <td>{{ number_format($row->price) }}</td>
                        <td>{{ $row->memo }}</td>
                        <td>{!! view($template, compact('row', 'gateKey', 'routeKey')) !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>

        // function init() {
        //     const form = $('#searchForm');
        //     common.initForm(form);

        //     $('#search').click(function () {
        //         form.submit();
        //     });
        //     $('#reset').click(function () {
        //         common.resetForm(form);
        //     });
        // }

        // init();
    </script>
@endsection
