@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('member_prices.index') }}">入會費資料</a></li>
        <li class="breadcrumb-item active">{{ $member_price->name }}</li>
        </ol>
    </nav>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">入會費資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">項目名稱</th>
                    <td>{{ $member_price->name }}</td>
                </tr>
                <tr>
                    <th>費用</th>
                    <td>{{ number_format($member_price->price) }}</td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>{{ $member_price->memo }}</td>
                </tr>
            </table>
        </div>
    </div>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


