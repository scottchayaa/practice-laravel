@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">會員管理</li>
        </ol>
    </nav>

    <div class="box">
        <form id="searchForm" method="GET">
        <div class="box-body">
            <div class="form-group">
                <label for="email" class="control-label col-md-1">Email：</label>
                <div class="col-md-2">
                    <input type="text" id="email" name="email" class="form-control" value="{{ request('email') }}" autofocus>
                </div>
            </div>
        </div>
        </form>
        <div class="box-footer">
            <button id="search" class="btn btn-info"><i class="fa fa-search"></i> 搜尋</button>
            <button id="reset" class="btn btn-warning"><i class="fa fa-eraser"></i> 清除</button>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            {{-- hearder... --}}
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Line uid</th>
                        <th>created_at</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($members as $row)
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->line->uid ?? '' }}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>
                            @can("{$gateKey}_edit")
                            <a href="/admin/members/{{ $row->id }}" class="btn btn-xs btn-info">
                                編輯
                            </a>
                            @endcan

                            @can("{$gateKey}_delete")
                            <form method="POST" action="/admin/members/{{ $row->id }}" style="display: inline-block;" onsubmit="return confirm('確定刪除?');">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input class="btn btn-xs btn-danger" type="submit" value="刪除">
                            </form>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            {!! $members->appends(request()->except('page'))->links() !!}
        </div>
    </div>
@endsection

@section('javascript')
<script>
    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });
    }

    $("#searchForm :input").keypress(function (event) {
        if (event.keyCode == 13) {
            $('#searchForm').submit();
        }
    });

    init();
</script>
@endsection
