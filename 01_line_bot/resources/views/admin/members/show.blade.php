@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('members.index') }}">會員管理</a></li>
            <li class="breadcrumb-item active">會員資料</li>
        </ol>
    </nav>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">會員資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">頭像</th>
                    <td></td>
                </tr>
                <tr>
                    <th>最後級數</th>
                    <td>{{ $member->level }}</td>
                </tr>
                <tr>
                    <th>會員編號</th>
                    <td>{{ $member->card_id }}</td>
                </tr>
                <tr>
                    <th>非會員編號</th>
                    <td>{{ $member->no_card_id }}</td>
                </tr>
                <tr>
                    <th>是否為會員</th>
                    <td>{{ $member->is_member?'是':'否' }}</td>
                </tr>
                <tr>
                    <th>姓名</th>
                    <td>{{ $member->name }}</td>
                </tr>
                <tr>
                    <th>性別</th>
                    <td>{{ $member->gender_str }}</td>
                </tr>
                <tr>
                    <th>出生日期</th>
                    <td>{{ $member->birthday }}</td>
                </tr>
                <tr>
                    <th>試學日期</th>
                    <td>{{ $member->first_try_at }}</td>
                </tr>
                <tr>
                    <th>入校日期</th>
                    <td>{{ $member->in_school_at }}</td>
                </tr>
                <tr>
                    <th>電話(H)</th>
                    <td>{{ $member->telephone_h }}</td>
                </tr>
                <tr>
                    <th>電話(O)</th>
                    <td>{{ $member->telephone_o }}</td>
                </tr>
                <tr>
                    <th>地址</th>
                    <td>{{ $member->address }}</td>
                </tr>
                <tr>
                    <th>行動電話(1)</th>
                    <td>{{ $member->cellphone_1 }}</td>
                </tr>
                <tr>
                    <th>行動電話(2)</th>
                    <td>{{ $member->cellphone_2 }}</td>
                </tr>
                <tr>
                    <th>緊急聯絡人姓名</th>
                    <td>{{ $member->emergency_name }}</td>
                </tr>
                <tr>
                    <th>緊急聯絡人電話</th>
                    <td>{{ $member->emergency_phone }}</td>
                </tr>
                <tr>
                    <th>監護人姓名</th>
                    <td>{{ $member->guardian_name }}</td>
                </tr>
                <tr>
                    <th>與監護人關係</th>
                    <td>{{ $member->guardian_relationship }}</td>
                </tr>
            </table>
        </div>

        <div class="box-footer">
            <p class="text-center">
                <a href="{{ route('members.edit', $member->id) }}" class="btn btn-info">
                    編輯
                </a>
            </p>
        </div>
    </div>

    <p>
        <a href="javascript:history.back()" class="btn btn-default">返 回</a>
    </p>

    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">歷史級數</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-md-1">繳費編號</th>
                        <th class="col-md-2">繳費時間</th>
                        <th class="col-md-2">報名班別</th>
                        <th class="col-md-2">報名月份</th>
                        <th>級數</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $row)
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ date('Y-m-d', strtotime($row->created_at)) }}</td>
                        <td>{!! $row->courses_name !!}</td>
                        <td>{{ $row->courses_month_select }}</td>
                        <td>{{ $row->courses_now_level }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <br>
    <br>
@endsection
