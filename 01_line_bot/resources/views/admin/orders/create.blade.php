@extends('layouts.app')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('orders.index') }}">繳費紀錄</a></li>
            <li class="breadcrumb-item active">新增</li>
        </ol>
    </nav>

    <form>
        {{csrf_field()}}

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="text-primary showTitle">
                    <strong>新增繳費資料</strong>
                </h3>
            </div>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="#tab_1" data-id="1" data-toggle="tab" aria-expanded="false"><b>[&nbsp;會&nbsp;員&nbsp;]</b></a>
                    </li>
                    <li>
                        <a href="#tab_2" data-id="2" data-toggle="tab" aria-expanded="false"><b>[&nbsp;非&nbsp;會&nbsp;員&nbsp;]</b></a>
                    </li>
                    <li>
                        <a href="#tab_3" data-id="3" data-toggle="tab" aria-expanded="false"><b>[&nbsp;新&nbsp;生&nbsp;]</b></a>
                    </li>
                    <li>
                        <a href="#tab_4" data-id="4" data-toggle="tab" aria-expanded="false"><b>[&nbsp;訪&nbsp;客&nbsp;]</b></a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane" id="tab_1">
                        <table class="table table-bordered">
                            <tr>
                                <th class="col-md-2">會員編號</th>
                                <td>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="card_id" value="{{ request('card_id') }}">
                                        <h6 class="text-danger">(請填入正確會員編號)</h6>
                                    </div>
                                </td>
                                <td class="col-md-4">
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="tab-pane" id="tab_2">
                        <table class="table table-bordered">
                            <tr>
                                <th class="col-md-2">非會員編號</th>
                                <td>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="no_card_id" value="{{ request('no_card_id') }}">
                                        <h6 class="text-danger">(請填入正確非會員編號)</h6>
                                    </div>
                                </td>
                                <td class="col-md-4">
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div class="tab-pane" id="tab_3">

                        <table class="table table-bordered">
                            <tr>
                                <th class="col-md-2">頭像</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>姓名 <span style="color:red">*<span></th>
                                <td>
                                    <input type="text" class="form-control" id="name">
                                </td>
                            </tr>
                            <tr>
                                <th>性別</th>
                                <td>
                                    <div class="radio">
                                        <label><input type="radio" name="gender" value="0" checked>女</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="gender" value="1">男</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>出生日期</th>
                                <td>
                                    <input type="text" class="form-control date" id="birthday" >
                                </td>
                            </tr>
                            <tr>
                                <th>試學日期 <span style="color:red">*<span></th>
                                <td>
                                    <input type="text" class="form-control date" id="first_try_at" >
                                </td>
                            </tr>
                            <tr>
                                <th>入校日期 <span style="color:red">*<span></th>
                                <td>
                                    <input type="text" class="form-control date" id="in_school_at">
                                </td>
                            </tr>
                            <tr>
                                <th>最後級數 <span style="color:red">*<span></th>
                                <td>
                                    <input type="number" class="form-control" id="level">
                                </td>
                            </tr>
                            <tr>
                                <th>電話(H)</th>
                                <td>
                                    <input type="text" class="form-control" id="telephone_h" >
                                </td>
                            </tr>
                            <tr>
                                <th>電話(O)</th>
                                <td>
                                    <input type="text" class="form-control" id="telephone_o" >
                                </td>
                            </tr>
                            <tr>
                                <th>地址</th>
                                <td>
                                    <input type="text" class="form-control" id="address" >
                                </td>
                            </tr>
                            <tr>
                                <th>行動電話(1)</th>
                                <td>
                                    <input type="text" class="form-control" id="cellphone_1" >
                                </td>
                            </tr>
                            <tr>
                                <th>行動電話(2)</th>
                                <td>
                                    <input type="text" class="form-control" id="cellphone_2" >
                                </td>
                            </tr>
                            <tr>
                                <th>緊急聯絡人姓名</th>
                                <td>
                                    <input type="text" class="form-control" id="emergency_name" >
                                </td>
                            </tr>
                            <tr>
                                <th>緊急聯絡人電話</th>
                                <td>
                                    <input type="text" class="form-control" id="emergency_phone" >
                                </td>
                            </tr>
                            <tr>
                                <th>監護人姓名</th>
                                <td>
                                    <input type="text" class="form-control" id="guardian_name" >
                                </td>
                            </tr>
                            <tr>
                                <th>與監護人關係</th>
                                <td>
                                    <input type="text" class="form-control" id="guardian_relationship" >
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div class="tab-pane" id="tab_4">
                    </div>
                </div>
            </div>

            <div class="box-body">

                <div id="block_member_prices" class="form-group">
                    <h4 class="text-primary showTitle">加入會員</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th class="col-md-2">選擇會費</th>
                            <td>
                                <div class="col-md-12">
                                    <select class="form-control" id="member_prices_id">
                                        <option data-price="0" value="">請選擇</option>
                                        @foreach ($member_prices as $row)
                                            <option data-price="{{$row->price}}" value="{{$row->id}}">{{$row->name . '  (' . number_format($row->price). ')'}}</option>
                                        @endforeach
                                    </select>
                                    <h6 id="member_prices_id_hint1" class="text-danger">(加入 → 產生會員編號; 暫不加入 → 產生非會員編號)</h6>
                                    <h6 id="member_prices_id_hint2" class="text-danger">(加入 → 產生會員編號)</h6>
                                </div>
                            </td>
                            <td class="col-md-4">
                                <strong id="member_price">0</strong>
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2">指定會員編號</th>
                            <td>
                                <div class="col-md-12">
                                    <input type="number" id="assign_card_id" class="form-control">
                                </div>
                            </td>
                            <td class="col-md-4">
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="block_courses" class="form-group">
                    <h4 class="text-primary showTitle">課程學費</h4>
                    <table class="table table-bordered"><tr>
                        <th class="col-md-2">目前級數</th>
                            <td>
                                <div class="col-md-12">
                                    <input type="number" id="courses_now_level" class="form-control" value="{{ request('level') }}">
                                </div>
                            </td>
                            <td class="col-md-4">
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2">選擇課程</th>
                            <td>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-success btn-xs btn-plus" onclick="clickBtnPlus(this)"><i class="fa fa-2x fa-plus"></i></button>
                                </div>

                                <div class="form-row items" style="display: none;">
                                    <div class="form-group col-md-10">
                                        <select id="courses_id" class="form-control">
                                            <option value="">請選擇</option>
                                            @foreach ($courses as $row)
                                            <option value="{{$row->id}}">{{$row->name . ' , ' . "($row->range_time)" }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button type="button" class="btn btn-danger btn-xs btn-minus" onclick="clickBtnMinus(this)"><i class="fa fa-2x fa-minus"></i></button>
                                    </div>
                                </div>
                            </td>
                            <td class="col-md-4">
                                <input type="number" id="courses_total_price" name="courses_total_price" class="form-control" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th>其他費用</th>
                            <td>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other1" value="配班">
                                        <label class="form-check-label" for="other1">配班</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other2" value="差補">
                                        <label class="form-check-label" for="other2">差補</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other3" value="轉班">
                                        <label class="form-check-label" for="other3">轉班</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other4" value="評量">
                                        <label class="form-check-label" for="other4">評量</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other5" value="延期">
                                        <label class="form-check-label" for="other5">延期</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other6" value="轉讓">
                                        <label class="form-check-label" for="other6">轉讓</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other7" value="自泳">
                                        <label class="form-check-label" for="other7">自泳</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other8" value="非會員入會金">
                                        <label class="form-check-label" for="other8">非會員入會金</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other9" value="其他:">
                                        <label class="form-check-label" for="other9">其他：</label>
                                        <input type="text" class="form-control" id="other9_text" value="" onchange="$('#other9').val('其他:'+this.value)">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <input type="number" id="courses_other_price" class="form-control" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th>折讓</th>
                            <td>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_rebate_memo[]" id="rebate1" value="折價券">
                                        <label class="form-check-label" for="rebate1">折價券</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_rebate_memo[]" id="rebate2" value="延費">
                                        <label class="form-check-label" for="rebate2">延費</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_rebate_memo[]" id="rebate3" value="打折">
                                        <label class="form-check-label" for="rebate3">打折(%)</label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <input type="number" id="courses_rebate_price" class="form-control" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th>報名月份</th>
                            <td>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        @for ($i = -3; $i <= 3; $i++)
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" name="courses_month_select[]" id="courses_month_select{{$i}}" value="{{date('Y-m', strtotime("last day of $i months"))}}">
                                            <label class="form-check-label" for="courses_month_select{{$i}}">{{date('Y-m', strtotime("last day of $i months"))}}</label>
                                        </div>
                                        @endfor
                                    </div>
                                    <div class="col-md-6">
                                        @for ($i = 4; $i <= 10; $i++)
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" name="courses_month_select[]" id="courses_month_select{{$i}}" value="{{date('Y-m', strtotime("last day of $i months"))}}">
                                            <label class="form-check-label" for="courses_month_select{{$i}}">{{date('Y-m', strtotime("last day of $i months"))}}</label>
                                        </div>
                                        @endfor
                                    </div>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th>學費備註</th>
                            <td>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="courses_memo">
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="block_products" class="form-group">
                    <h4 class="text-primary showTitle">加購商品</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th class="col-md-2">選擇商品</th>
                            <td>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        商品
                                    </div>
                                    <div class="col-md-6">
                                        數量
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="button" class="btn btn-success btn-xs btn-plus" onclick="clickBtnPlus(this)"><i class="fa fa-2x fa-plus"></i></button>
                                </div>

                                <div class="form-row items" style="display: none;">
                                    <div class="form-group col-md-6">
                                        <select id="products_id" name="products_id" class="form-control" onchange="caculation();">
                                            <option data-price="0" value="">請選擇</option>
                                            @foreach ($products as $row)
                                            <option data-price="{{$row->price}}" value="{{$row->id}}">{{$row->name . ' (' . number_format($row->price) . ')'}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <select id="products_quantity" name="products_id" class="form-control" onchange="caculation();">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button type="button" class="btn btn-danger btn-xs btn-minus" onclick="clickBtnMinus(this)"><i class="fa fa-2x fa-minus"></i></button>
                                    </div>
                                </div>
                            </td>
                            <td class="col-md-4">
                                <span id="products_total_price">0<span>
                            </td>
                        </tr>
                        <tr>
                            <th>折讓</th>
                            <td>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="products_rebate_memo[]" id="product_rebate1" value="折價券">
                                        <label class="form-check-label" for="product_rebate1">折價券</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="products_rebate_memo[]" id="product_rebate2" value="打折">
                                        <label class="form-check-label" for="product_rebate2">打折(%)</label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <input type="number" id="products_rebate_price" name="products_rebate_price" class="form-control" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th>商品備註</th>
                            <td>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="products_memo" value="">
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="block_checkout" class="form-group">
                    <h4 class="text-danger showTitle">結帳</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th class="col-md-2">狀態</th>
                            <td>
                                <div class="col-md-12">
                                    <div class="form-row">
                                        <select id="status" class="form-control">
                                            <option value="0">未付款</option>
                                            <option value="1">已預付</option>
                                            <option value="2">完成付款</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td class="col-md-4">
                            </td>
                        </tr>
                        <tr>
                            <th>繳費備註</th>
                            <td>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="memo" value="">
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2" class="text-right"><h4>合計</h4></th>
                            <td>
                                <h4 class="text-danger"><strong id="total" data-total=""></strong></h4>
                                {{-- <i class="fa fa-lg fa-edit text-primary"></i> --}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="box-footer">
                <p class="text-center">
                    <button id="btnSave" type="button" class="btn btn-danger" data-toggle="modal" data-target="#mPreCheckout" onclick="preCheckout();">結帳</button>
                </p>
            </div>
        </div>
    </form>


<!-- Modal : Preview Checkout -->
<div id="mPreCheckout" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">結帳確認</h4>
            </div>
            <div class="modal-body">
                Preview
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-danger" onclick="checkout()" data-dismiss="modal">送出</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Checkout modal-danger Fail -->
<div id="mCheckoutFail" class="modal modal-danger fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">結帳失敗</h4>
            </div>
            <div class="modal-body">
                Error
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">關閉</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Checkout Success -->
<div id="mCheckoutSuccess" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">結帳成功</h4>
            </div>
            <div class="modal-body">
                Success
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="/js/DateFormat.js"></script>
<script src="/js/queryString.js"></script>
<script>
    let NOW_TAB = -1;

    // 結帳確認
    function preCheckout() {
        let modal = $('#mPreCheckout .modal-body').html('').append($('.box').clone());
        modal.find('.box').attr('class', '');
        modal.find('.nav-tabs a, .box-body, .tab-content').css('background-color', '#ddd');

        // fix jquery clone select bug ...
        var selects = $('form').find("select");
        $(selects).each(function(i) {
            modal.find("select").eq(i).val($(this).val());
        });

        modal.find('.box-header').remove();
        modal.find('.nav-tabs li[class!="active"]').remove();
        modal.find('.tab-pane:not(".active")').remove();
        modal.find('.btn-plus, .btn-minus').remove();
        modal.find('.box-footer').remove();

        modal.find('input, select').attr('disabled', true);
    }

    function checkout() {
        // 結帳類型 : 會員, 非會員, 新生, 訪客
        // 會員
        if (NOW_TAB == 1) {
            let order = {
                type: 1,
                card_id: $('#card_id').val(),
                payable_price: $('#total').data('total'),
                status: $('form #status').val(),
                memo: $('form #memo').val(),

                courses_now_level: $('form #courses_now_level').val(),
                courses_other_memo: $('form input[name="courses_other_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                courses_other_price: $('form #courses_other_price').val(),
                courses_rebate_memo: $('form input[name="courses_rebate_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                courses_rebate_price: $('form #courses_rebate_price').val(),
                courses_total_price: $('form #courses_total_price').val(),
                courses_memo: $('form #courses_memo').val(),
                courses_month_select: $('form input[name="courses_month_select[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),

                products_rebate_memo: $('form input[name="products_rebate_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                products_rebate_price: $('form #products_rebate_price').val(),
                products_memo: $('form #products_memo').val(),
            };

            let order_courses_details = [];
            $('form #courses_id ').filter(function () {
                return this.value; // find the items which are selected
            }).each(function () {
                order_courses_details.push({
                    courses_id: this.value,
                });
            });

            let order_products_details = [];
            $('form #products_id ').filter(function () {
                return this.value; // find the items which are selected
            }).each(function () {
                order_products_details.push({
                    products_id: this.value,
                    quantity: $(this).closest('.form-row').find('#products_quantity').val()
                });
            });

            // console.log(order);
            // console.log(order_courses_details);
            // console.log(order_products_details);

            // checkout api : member
            $.post('{{ route("orders.checkoutMember") }}', {
                order: order,
                order_courses_details: order_courses_details,
                order_products_details: order_products_details
            }).done(function (res){
                console.log('成功');
                console.log(res);
                location.href = '{{ route("members.index") }}';
            }).fail(function (error){
                console.log(error.status + ' 失敗');
                console.log(error.responseJSON);
                $('#mCheckoutFail').modal('show');
                $('#mCheckoutFail .modal-body').html(error.responseJSON.message);
            });
        }
        // 非會員
        if (NOW_TAB == 2) {
            let order = {
                type: 2,
                no_card_id: $('#no_card_id').val(),
                payable_price: $('#total').data('total'),
                status: $('form #status').val(),
                memo: $('form #memo').val(),

                member_prices_id: $('form #member_prices_id').val(),
                assign_card_id: $('form #assign_card_id').val(),

                courses_now_level: $('form #courses_now_level').val(),
                courses_other_memo: $('form input[name="courses_other_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                courses_other_price: $('form #courses_other_price').val(),
                courses_rebate_memo: $('form input[name="courses_rebate_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                courses_rebate_price: $('form #courses_rebate_price').val(),
                courses_total_price: $('form #courses_total_price').val(),
                courses_memo: $('form #courses_memo').val(),
                courses_month_select: $('form input[name="courses_month_select[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),

                products_rebate_memo: $('form input[name="products_rebate_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                products_rebate_price: $('form #products_rebate_price').val(),
                products_memo: $('form #products_memo').val(),
            };

            let order_courses_details = [];
            $('form #courses_id ').filter(function () {
                return this.value; // find the items which are selected
            }).each(function () {
                order_courses_details.push({
                    courses_id: this.value,
                });
            });

            let order_products_details = [];
            $('form #products_id ').filter(function () {
                return this.value; // find the items which are selected
            }).each(function () {
                order_products_details.push({
                    products_id: this.value,
                    quantity: $(this).closest('.form-row').find('#products_quantity').val()
                });
            });

            // console.log(order);
            // console.log(order_courses_details);
            // console.log(order_products_details);

            // checkout api : none-member
            $.post('{{ route("orders.checkoutNoneMember") }}', {
                order: order,
                order_courses_details: order_courses_details,
                order_products_details: order_products_details
            }).done(function (res){
                console.log('成功');
                console.log(res);
                location.href = '{{ route("members.index") }}';
            }).fail(function (error){
                console.log(error.status + ' 失敗');
                console.log(error.responseJSON);
                $('#mCheckoutFail').modal('show');
                $('#mCheckoutFail .modal-body').html(error.responseJSON.message);
            });
        }
        // 新生
        if (NOW_TAB == 3) {
            let member = {
                name: $('#name').val(),
                gender: $('input[name="gender"]:checked').val(),
                birthday: $('#birthday').val(),
                first_try_at: $('#first_try_at').val(),
                in_school_at: $('#in_school_at').val(),
                level: $('#level').val(),
                telephone_h: $('#telephone_h').val(),
                telephone_o: $('#telephone_o').val(),
                address: $('#address').val(),
                cellphone_1: $('#cellphone_1').val(),
                cellphone_2: $('#cellphone_2').val(),
                emergency_name: $('#emergency_name').val(),
                emergency_phone: $('#emergency_phone').val(),
                guardian_name: $('#guardian_name').val(),
                guardian_relationship: $('#guardian_relationship').val(),
            };

            let order = {
                type: 3,
                no_card_id: $('#no_card_id').val(),
                payable_price: $('#total').data('total'),
                status: $('form #status').val(),
                memo: $('form #memo').val(),

                member_prices_id: $('form #member_prices_id').val(),
                assign_card_id: $('form #assign_card_id').val(),

                courses_now_level: $('form #courses_now_level').val(),
                courses_other_memo: $('form input[name="courses_other_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                courses_other_price: $('form #courses_other_price').val(),
                courses_rebate_memo: $('form input[name="courses_rebate_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                courses_rebate_price: $('form #courses_rebate_price').val(),
                courses_total_price: $('form #courses_total_price').val(),
                courses_memo: $('form #courses_memo').val(),
                courses_month_select: $('form input[name="courses_month_select[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),

                products_rebate_memo: $('form input[name="products_rebate_memo[]"]:checked').map(function (index, element) {
                    return $(this).val();
                }).get().join(','),
                products_rebate_price: $('form #products_rebate_price').val(),
                products_memo: $('form #products_memo').val(),
            };

            let order_courses_details = [];
            $('form #courses_id ').filter(function () {
                return this.value; // find the items which are selected
            }).each(function () {
                order_courses_details.push({
                    courses_id: this.value,
                });
            });

            let order_products_details = [];
            $('form #products_id ').filter(function () {
                return this.value; // find the items which are selected
            }).each(function () {
                order_products_details.push({
                    products_id: this.value,
                    quantity: $(this).closest('.form-row').find('#products_quantity').val()
                });
            });

            // console.log(member);
            // console.log(order);
            // console.log(order_courses_details);
            // console.log(order_products_details);

            // checkout api : new-member
            $.post('{{ route("orders.checkoutNewMember") }}', {
                member: member,
                order: order,
                order_courses_details: order_courses_details,
                order_products_details: order_products_details
            }).done(function (res){
                console.log('成功');
                console.log(res);
                location.href = '{{ route("members.index") }}';
            }).fail(function (error){
                console.log(error.status + ' 失敗');
                console.log(error.responseJSON);
                $('#mCheckoutFail').modal('show');
                if (error.status == 422) {
                    $('#mCheckoutFail .modal-body').html(JSON.stringify(error.responseJSON.errors));
                } else {
                    $('#mCheckoutFail .modal-body').html(error.responseJSON.message);
                }
            });
        }
        // 訪客
        if (NOW_TAB == 4) {
            let order = {
                type: 4,
                payable_price: $('#total').data('total'),
                status: $('form #status').val(),
                memo: $('form #memo').val(),

                products_rebate_memo: $('form input[name="products_rebate_memo[]"]:checked').map(function (index, element) {
                    console.log(index);
                    return $(this).val();
                }).get().join(','),
                products_rebate_price: $('form #products_rebate_price').val(),
                products_memo: $('form #products_memo').val(),
            };

            let order_products_details = [];
            for (let i = 0; i < $('form #products_id').length - 1; i++) {
                let row = {
                    products_id: $('form #products_id').eq(i).val(),
                    quantity: $('form #products_quantity').eq(i).val()
                };
                order_products_details.push(row);
            }

            // checkout api : guest
            $.post('{{ route("orders.checkoutGuest") }}', {
                order: order,
                order_products_details: order_products_details
            }).done(function (res){
                console.log('成功');
                console.log(res);
                location.href = '{{ route("orders.index") }}';
            }).fail(function (error){
                console.log(error.status + ' 失敗');
                console.log(error.responseJSON);
                $('#mCheckoutFail').modal('show');
                $('#mCheckoutFail .modal-body').html(error.responseJSON.message);
            });
        }
    }

    function init() {
        // Setting default tab by query string "tab"
        NOW_TAB = parseInt(queryString.get('tab'));
        if (NOW_TAB >= 1 && NOW_TAB <= 4) {
            $('.nav-tabs li').eq(NOW_TAB - 1).addClass('active');
            $('.tab-pane').eq(NOW_TAB - 1).addClass('active');
            showBlock(NOW_TAB);
        } else {
            $('.nav-tabs li').eq(0).addClass('active');
            $('.tab-pane').eq(0).addClass('active');
            showBlock(1);
        }

        // Event: Member Price change on <select>
        $('#member_prices_id').change(function() {
            let member_price = $(this).find(':selected').data('price');
            $('#member_price').text(member_price);
            caculation();
        });

        // Event: price change on textbox (Course, Products)
        $('#courses_total_price, #courses_rebate_price, #courses_other_price, #products_rebate_price').change(function() {
            caculation();
        });

        // Event: Tab change
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            NOW_TAB = $(e.target).data("id"); // activated tab
            console.log(NOW_TAB);
            showBlock(NOW_TAB);
            caculation();
        });

        // init trigger click : .btn-plus button
        $('.btn-plus').click();

        caculation();
    }

    function showBlock(tabId) {
        if (tabId == 1) {
            $('#block_member_prices').hide();
            $('#block_courses').show();
        } else if (tabId == 2) {
            $('#block_member_prices').show();
            $('#block_courses').show();
            $('#member_prices_id_hint1').hide(); $('#member_prices_id_hint2').show();
        } else if (tabId == 3) {
            $('#block_member_prices').show();
            $('#block_courses').show();
            $('#member_prices_id_hint1').show(); $('#member_prices_id_hint2').hide();
        } else if (tabId == 4) {
            $('#block_member_prices').hide();
            $('#block_courses').hide();
        }
    }

    function clickBtnPlus(element) {
        $(element).closest('div').before($(element).closest('td').find('.items').last().clone().show());
    }

    function clickBtnMinus(element) {
        $(element).closest('.items').remove();
        caculation();
    }

    function caculation_products() {
        let products_total_price = 0;
        for (let i = 0; i < $('form #products_id').length - 1; i++) {
            let price = parseInt($('form #products_id option:selected').eq(i).data('price'));
            let quantity = parseInt($('form #products_quantity option:selected').eq(i).val());
            products_total_price += price * quantity;
        }
        $('#products_total_price').text(products_total_price);
    }

    function caculation() {
        let total = 0;

        // members price
        let member_price = $('#member_price').is(':visible')
        ? parseInt($('#member_price').text())
        : 0;

        // courses price
        let courses_total_price = $('#courses_total_price').is(':visible')
        ? parseInt($("#courses_total_price ").val())
        : 0;
        let courses_rebate_price = $('#courses_rebate_price').is(':visible')
        ? parseInt($("#courses_rebate_price ").val())
        : 0;
        let courses_other_price = $('#courses_other_price').is(':visible')
        ? parseInt($("#courses_other_price ").val())
        : 0;

        // products price
        caculation_products();
        let products_total_price = parseInt($("#products_total_price").text());
        let products_rebate_price = parseInt($("#products_rebate_price").val());

        total = member_price + courses_total_price + courses_rebate_price + courses_other_price + products_total_price + products_rebate_price;
        $("#total").attr('data-total', total).html('NTD ' + common.formatNumber(total));

        if(total <= 0 ) {
            $('#btnSave').addClass('disabled');
        } else {
            $('#btnSave').removeClass('disabled');
        }
    }

    init();
</script>
@endsection
