@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">繳費紀錄</li>
        </ol>
    </nav>

    <div class="box">
        <form id="searchForm" method="GET">
        <div class="box-body">
            <div class="row form-group">
                <label for="card_id" class="control-label col-md-1">會員編號：</label>
                <div class="col-md-2">
                    <input type="text" name="card_id" class="form-control" value="{{ request('card_id') }}" autofocus>
                </div>
                <label for="no_card_id" class="control-label col-md-1">非會員編號：</label>
                <div class="col-md-2">
                    <input type="text" name="no_card_id" class="form-control" value="{{ request('no_card_id') }}">
                </div>
            </div>
            <div class="row form-group">
                <label for="member_name" class="control-label col-md-1">會員名字：</label>
                <div class="col-md-2">
                    <input type="text" name="member_name" class="form-control" value="{{ request('member_name') }}">
                </div>
                <label for="start_created_at" class="control-label col-md-1">繳費時間(起)：</label>
                <div class="col-md-2">
                    <input type="text" name="start_created_at" class="form-control date" value="{{ request('start_created_at') }}">
                </div>
                <label for="start_created_at" class="control-label col-md-1">繳費時間(訖)：</label>
                <div class="col-md-2">
                    <input type="text" name="end_created_at" class="form-control date" value="{{ request('end_created_at') }}">
                </div>
            </div>
        </div>
        </form>
        <div class="box-footer">
            <button id="search" class="btn btn-info"><i class="fa fa-search"></i> 搜尋</button>
            <button id="reset" class="btn btn-warning"><i class="fa fa-eraser"></i> 清除</button>
        </div>
    </div>

    @can('order_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('orders.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('orders.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            @can('order_create')
            <a href="{{ route('orders.create') }}?tab=4" class="btn btn-default">
                <i class="fa fa-plus"></i> 新增
            </a>
            @endcan
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>編號</th>
                        <th class="col-md-1">類型</th>
                        <th class="col-md-1">繳費時間</th>
                        <th class="col-md-2">會員名字</th>
                        <th>總繳費金額</th>
                        <th>狀態</th>
                        <th class="col-md-3">繳費備註</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $row)
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->type_str }}</td>
                        <td>
                            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="{{ $row->created_at }}">
                                {{ $row->created_at->diffForHumans() }}
                            </span>
                        </td>
                        <td>{{ $row->member_name }}</td>
                        <td>{{ $row->payable_price }}</td>
                        <td>{{ $row->status_str }}</td>
                        <td>{{ $row->memo }}</td>
                        <td>{!! view($template, compact('row', 'gateKey', 'routeKey')) !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            {!! $orders->appends(request()->except('page'))->links() !!}
        </div>
    </div>
@endsection

@section('javascript')
    <script>

        function init() {
            const form = $('#searchForm');
            common.initForm(form);

            $('#search').click(function () {
                form.submit();
            });
            $('#reset').click(function () {
                common.resetForm(form);
            });
        }

        init();
    </script>
@endsection
