@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('orders.index') }}">繳費紀錄</a></li>
            <li class="breadcrumb-item active">繳費資料</li>
        </ol>
    </nav>

    <div id='top'></div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">繳費資料</h3>{{-- @lang('quickadmin.qa_view') --}}
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">會員</th>
                    <td>{{ $order->member_name }}</td>
                </tr>
                <tr>
                    <th>繳費時間</th>
                    <td>{{ $order->created_at }}</td>
                </tr>
                <tr>
                    <th>狀態</th>
                    <td>{{ $order->status_str }}</td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>{{ $order->memo }}</td>
                </tr>
                <tr>
                    <th>總繳費金額</th>
                    <td class="text-danger"><h4><b>NTD {{ number_format($order->payable_price) }}</b></h4></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mOrder" >編輯</button>
            </div>
        </div>
    </div>

    @if ($order->member_prices_id != null)
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">繳費明細 : 入會費</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">項目</th>
                    <td>{{ $order->MemberPrice->name }}</td>
                    <td class="col-md-4">{{ number_format($order->MemberPrice->price) }}</td>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">合計</th>
                    <td colspan="2">NTD {{ number_format($order->MemberPrice->price) }}</td>
                </tr>
            </table>
        </div>
    </div>
    @endif

    @if (count($order->OrderCoursesDetails))
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">繳費明細 : 課程</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th>目前級數</th>
                    <td colspan="2">{{ $order->courses_now_level }}</td>
                </tr>
                <tr>
                    <th class="col-md-2">課程</th>
                    <td>
                        @foreach ($order->OrderCoursesDetails as $idx => $detail)
                        <p>
                            {!! "(" . ($idx + 1 ) . ") " . $detail->Course->name !!}
                        </p>
                        @endforeach
                    </td>
                    <td class="col-md-4">{{ number_format($order->courses_total_price) }}</td>
                </tr>
                <tr>
                    <th>其他費用</th>
                    <td>{!! $order->courses_other_memo !!}</td>
                    <td>{{ number_format($order->courses_other_price) }}</td>
                </tr>
                <tr>
                    <th>折讓</th>
                    <td>{!! $order->courses_rebate_memo !!}</td>
                    <td>{{ number_format($order->courses_rebate_price) }}</td>
                </tr>
                <tr>
                    <th>課程備註</th>
                    <td colspan="2">{!! $order->courses_memo !!}</td>
                </tr>
                <tr>
                    <th>報名月份</th>
                    <td colspan="2">{{ $order->courses_month_select }}</td>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">合計</th>
                    <td colspan="2">NTD {{ number_format($order->courses_total_price + $order->courses_other_price + $order->courses_rebate_price) }}</td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mCourses">編輯</button>
            </div>
        </div>
    </div>
    @endif

    <?php $products_price = 0; ?>
    @if (count($order->OrderProductsDetails))
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">繳費明細 : 商品</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">商品</th>
                    <td>
                        @foreach ($order->OrderProductsDetails as $idx => $detail)
                        <div class="row form-group">
                            <div class="col-md-6">
                                {!! "(" . ($idx + 1 ) . ") " . $detail->Product->name !!}
                                <small style="color:#999">({{ $detail->Product->model }}/{{ $detail->Product->size }}/{{ $detail->Product->color }})</small>
                            </div>
                            <div class="col-md-6">
                                {!! $detail->Product->price . ' * ' . $detail->quantity !!}
                            </div>
                        </div>
                        <?php $products_price += $detail->Product->price * $detail->quantity; ?>
                        @endforeach
                    </td>
                    <td class="col-md-4">
                        {{ number_format($products_price) }}
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">折讓</th>
                    <td>{{ $order->products_rebate_memo }}</td>
                    <td>{{ number_format($order->products_rebate_price) }}</td>
                </tr>
                <tr>
                    <th>商品備註</th>
                    <td colspan="2">{!! $order->products_memo !!}</td>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">合計</th>
                    <td colspan="2">NTD {{ number_format($products_price + $order->products_rebate_price) }}</td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mProducts">編輯</button>
            </div>
        </div>
    </div>
    @endif

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>

<!-- Modal : Order -->
<div id="mOrder" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">編輯繳費資料</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>狀態</th>
                        <td>
                            <select id="status" class="form-control">
                                <option value="0" {{ $order->status == 0 ? 'selected':'' }}>未付款</option>
                                <option value="1" {{ $order->status == 1 ? 'selected':'' }}>已預付</option>
                                <option value="2" {{ $order->status == 2 ? 'selected':'' }}>完成付款</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" value="{{ $order->memo }}">
                        </td>
                    </tr>
                    <tr>
                        <th>總繳費金額</th>
                        <td>
                            <input id="payable_price" class="form-control" value="{{ $order->payable_price }}">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-danger" onclick="updateOrder()" data-dismiss="modal">更新</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Courses -->
<div id="mCourses" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">編輯繳費明細 : 課程</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th class="col-md-2">目前級數</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" id="courses_now_level" class="form-control" value="{{ $order->courses_now_level }}">
                            </div>
                        </td>
                        <td class="col-md-3">
                        </td>
                    </tr>
                    <tr>
                        <th>選擇課程</th>
                        <td>
                            @foreach ($order_courses_detail as $detail)
                            <div class="form-row items">
                                <div class="form-group col-md-10">
                                    <select id="courses_id" class="form-control">
                                        <option value="">請選擇</option>
                                        @foreach ($courses as $row)
                                        <option value="{{$row->id}}" {{ $row->id == $detail->courses_id ? 'selected':'' }}>{{$row->name . ' , ' . "($row->range_time)" }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="button" class="btn btn-danger btn-xs btn-minus" onclick="clickBtnMinus(this)"><i class="fa fa-2x fa-minus"></i></button>
                                </div>
                            </div>
                            @endforeach

                            <div class="col-md-12">
                                <button type="button" class="btn btn-success btn-xs btn-plus" onclick="clickBtnPlus(this)"><i class="fa fa-2x fa-plus"></i></button>
                            </div>

                            <div class="form-row items" style="display: none;">
                                <div class="form-group col-md-10">
                                    <select id="courses_id" class="form-control">
                                        <option value="">請選擇</option>
                                        @foreach ($courses as $row)
                                        <option value="{{$row->id}}">{{$row->name . ' , ' . "($row->range_time)" }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="button" class="btn btn-danger btn-xs btn-minus" onclick="clickBtnMinus(this)"><i class="fa fa-2x fa-minus"></i></button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="number" id="courses_total_price" name="courses_total_price" class="form-control" value="{{ $order->courses_total_price }}">
                        </td>
                    </tr>
                    <tr>
                        <th>其他費用</th>
                        <td>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other1" value="配班" {{ Str::contains($order->courses_other_memo, '配班')?'checked':'' }}>
                                    <label class="form-check-label" for="other1">配班</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other2" value="差補" {{ Str::contains($order->courses_other_memo, '差補')?'checked':'' }}>
                                    <label class="form-check-label" for="other2">差補</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other3" value="轉班" {{ Str::contains($order->courses_other_memo, '轉班')?'checked':'' }}>
                                    <label class="form-check-label" for="other3">轉班</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other4" value="評量" {{ Str::contains($order->courses_other_memo, '評量')?'checked':'' }}>
                                    <label class="form-check-label" for="other4">評量</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other5" value="延期" {{ Str::contains($order->courses_other_memo, '延期')?'checked':'' }}>
                                    <label class="form-check-label" for="other5">延期</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other6" value="轉讓" {{ Str::contains($order->courses_other_memo, '轉讓')?'checked':'' }}>
                                    <label class="form-check-label" for="other6">轉讓</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other7" value="自泳" {{ Str::contains($order->courses_other_memo, '自泳')?'checked':'' }}>
                                    <label class="form-check-label" for="other7">自泳</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other8" value="非會員入會金" {{ Str::contains($order->courses_other_memo, '非會員入會金')?'checked':'' }}>
                                    <label class="form-check-label" for="other8">非會員入會金</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_other_memo[]" id="other9" value="其他:{{ Str::contains($order->courses_other_memo, '其他:')?Str::after($order->courses_other_memo, '其他:'):'' }}" {{ Str::contains($order->courses_other_memo, '其他:')?'checked':'' }}>
                                    <label class="form-check-label" for="other9">其他：</label>
                                    <input type="text" class="form-control" id="other9_text" value="{{ Str::contains($order->courses_other_memo, '其他:')?Str::after($order->courses_other_memo, '其他:'):'' }}" onchange="$('#other9').val('其他:'+this.value)">
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="number" id="courses_other_price" class="form-control" value="{{ $order->courses_other_price }}">
                        </td>
                    </tr>
                    <tr>
                        <th>折讓</th>
                        <td>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_rebate_memo[]" id="rebate1" value="折價券"  {{ Str::contains($order->courses_rebate_memo, '折價券')?'checked':'' }}>
                                    <label class="form-check-label" for="rebate1">折價券</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_rebate_memo[]" id="rebate2" value="延費" {{ Str::contains($order->courses_rebate_memo, '延費')?'checked':'' }}>
                                    <label class="form-check-label" for="rebate2">延費</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="courses_rebate_memo[]" id="rebate3" value="打折" {{ Str::contains($order->courses_rebate_memo, '打折')?'checked':'' }}>
                                    <label class="form-check-label" for="rebate3">打折(%)</label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="number" id="courses_rebate_price" class="form-control" value="{{ $order->courses_rebate_price }}">
                        </td>
                    </tr>
                    <tr>
                        <th>報名月份</th>
                        <td>
                            <div class="form-row">
                                <div class="col-md-6">
                                    @for ($i = -3; $i <= 3; $i++)
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_month_select[]" id="courses_month_select{{$i}}" value="{{date('Y-m', strtotime("{$order->created_at} last day of $i months"))}}" {{ Str::contains($order->courses_month_select, date('Y-m', strtotime("{$order->created_at} last day of $i months")))?'checked':'' }}>
                                        <label class="form-check-label" for="courses_month_select{{$i}}">{{date('Y-m', strtotime("{$order->created_at} last day of $i months"))}}</label>
                                    </div>
                                    @endfor
                                </div>
                                <div class="col-md-6">
                                    @for ($i = 4; $i <= 10; $i++)
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="courses_month_select[]" id="courses_month_select{{$i}}" value="{{date('Y-m', strtotime("{$order->created_at} last day of $i months"))}}" {{ Str::contains($order->courses_month_select, date('Y-m', strtotime("{$order->created_at} last day of $i months")))?'checked':'' }}>
                                        <label class="form-check-label" for="courses_month_select{{$i}}">{{date('Y-m', strtotime("{$order->created_at} last day of $i months"))}}</label>
                                    </div>
                                    @endfor
                                </div>
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <th>學費備註</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="courses_memo" value="{{ $order->courses_memo }}">
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="modal-footer">
                <div id="payable_price" style="display:none;">{{ $order->payable_price }}</div>
                <p class="text-center">
                    <button type="button" class="btn btn-danger" onclick="updateCoursesDetail()" data-dismiss="modal">更新</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Products -->
<div id="mProducts" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">編輯繳費明細 : 商品</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th class="col-md-2">商品</th>
                        <td>
                            <div class="form-row">
                                <div class="col-md-7">
                                    商品
                                </div>
                                <div class="col-md-5">
                                    數量
                                </div>
                            </div>

                            @foreach ($order_products_detail as $detail)
                            <div class="form-row items">
                                <div class="form-group col-md-7">
                                    <select id="products_id" name="products_id" class="form-control" onchange="caculation_products();">
                                        <option data-price="0" value="">請選擇</option>
                                        @foreach ($products as $row)
                                        <option data-price="{{$row->price}}" value="{{$row->id}}" {{ $detail->products_id == $row->id ? 'selected': '' }}>{{$row->name . ' (' . number_format($row->price) . ')'}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <select id="products_quantity" name="products_id" class="form-control" onchange="caculation_products();">
                                        <option value="1" {{ $detail->quantity == 1 ? 'selected': '' }}>1</option>
                                        <option value="2" {{ $detail->quantity == 2 ? 'selected': '' }}>2</option>
                                        <option value="3" {{ $detail->quantity == 3 ? 'selected': '' }}>3</option>
                                        <option value="4" {{ $detail->quantity == 4 ? 'selected': '' }}>4</option>
                                        <option value="5" {{ $detail->quantity == 5 ? 'selected': '' }}>5</option>
                                        <option value="6" {{ $detail->quantity == 6 ? 'selected': '' }}>6</option>
                                        <option value="7" {{ $detail->quantity == 7 ? 'selected': '' }}>7</option>
                                        <option value="8" {{ $detail->quantity == 8 ? 'selected': '' }}>8</option>
                                        <option value="9" {{ $detail->quantity == 9 ? 'selected': '' }}>9</option>
                                        <option value="10" {{ $detail->quantity == 10 ? 'selected': '' }}>10</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="button" class="btn btn-danger btn-xs btn-minus" onclick="clickBtnMinus(this)"><i class="fa fa-2x fa-minus"></i></button>
                                </div>
                            </div>
                            @endforeach

                            <div class="col-md-12">
                                <button type="button" class="btn btn-success btn-xs btn-plus" onclick="clickBtnPlus(this)"><i class="fa fa-2x fa-plus"></i></button>
                            </div>

                            <div class="form-row items" style="display: none;">
                                <div class="form-group col-md-7">
                                    <select id="products_id" name="products_id" class="form-control" onchange="caculation_products();">
                                        <option data-price="0" value="">請選擇</option>
                                        @foreach ($products as $row)
                                        <option data-price="{{$row->price}}" value="{{$row->id}}">{{$row->name . ' (' . number_format($row->price) . ')'}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <select id="products_quantity" name="products_id" class="form-control" onchange="caculation_products();">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="button" class="btn btn-danger btn-xs btn-minus" onclick="clickBtnMinus(this)"><i class="fa fa-2x fa-minus"></i></button>
                                </div>
                            </div>
                        </td>
                        <td class="col-md-2">
                            <span id="products_total_price">{{ $products_price }}<span>
                        </td>
                    </tr>
                    <tr>
                        <th>折讓</th>
                        <td>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="products_rebate_memo[]" id="product_rebate1" value="折價券" {{ Str::contains($order->products_rebate_memo, '折價券')?'checked':'' }}>
                                    <label class="form-check-label" for="product_rebate1">折價券</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="products_rebate_memo[]" id="product_rebate2" value="打折" {{ Str::contains($order->products_rebate_memo, '打折')?'checked':'' }}>
                                    <label class="form-check-label" for="product_rebate2">打折(%)</label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="number" id="products_rebate_price" class="form-control" value="{{ $order->products_rebate_price }}">
                        </td>
                    </tr>
                    <tr>
                        <th>商品備註</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="products_memo" value="{{ $order->products_memo }}">
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-danger" onclick="updateProductsDetail()" data-dismiss="modal">更新</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Update Fail Message -->
<div id="mFail" class="modal modal-danger fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">更新失敗</h4>
                </div>
                <div class="modal-body">
                    Error
                </div>
                <div class="modal-footer">
                    <p class="text-center">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">關閉</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
<script>
    function updateOrder() {
        let order = {
            payable_price: $('#mOrder #payable_price').val(),
            status: $('#mOrder #status').val(),
            memo: $('#mOrder #memo').val(),
        }

        $.post('{{ route('orders.updateOrder', $order->id) }}', {
            order: order
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function updateCoursesDetail() {
        let order = {
            courses_now_level: $('#mCourses #courses_now_level').val(),
            courses_other_memo: $('#mCourses input[name="courses_other_memo[]"]:checked').map(function (index, element) {
                return $(this).val();
            }).get().join(','),
            courses_other_price: $('#mCourses #courses_other_price').val(),
            courses_rebate_memo: $('#mCourses input[name="courses_rebate_memo[]"]:checked').map(function (index, element) {
                return $(this).val();
            }).get().join(','),
            courses_rebate_price: $('#mCourses #courses_rebate_price').val(),
            courses_total_price: $('#mCourses #courses_total_price').val(),
            courses_memo: $('#mCourses #courses_memo').val(),
            courses_month_select: $('#mCourses input[name="courses_month_select[]"]:checked').map(function (index, element) {
                return $(this).val();
            }).get().join(','),
        }

        let order_courses_details = [];
        $('#mCourses #courses_id ').filter(function () {
            return this.value; // find the items which are selected
        }).each(function () {
            order_courses_details.push({
                courses_id: this.value,
            });
        });

        $.post('{{ route('orders.updateCoursesDetail', $order->id) }}', {
            order: order,
            order_courses_details: order_courses_details
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function updateProductsDetail() {
        let order = {
            products_rebate_memo: $('#mProducts input[name="products_rebate_memo[]"]:checked').map(function (index, element) {
                return $(this).val();
            }).get().join(','),
            products_rebate_price: $('#mProducts #products_rebate_price').val(),
            products_memo: $('#mProducts #products_memo').val(),
        }

        let order_products_details = [];
        $('#mProducts #products_id ').filter(function () {
            return this.value; // find the items which are selected
        }).each(function () {
            order_products_details.push({
                products_id: this.value,
                quantity: $(this).closest('.form-row').find('#products_quantity').val()
            });
        });

        $.post('{{ route('orders.updateProductsDetail', $order->id) }}', {
            order: order,
            order_products_details: order_products_details
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function init() {

    }

    function clickBtnPlus(element) {
        $(element).closest('div').before($(element).closest('td').find('.items').last().clone().show());
    }

    function clickBtnMinus(element) {
        $(element).closest('.items').remove();
        caculation_products();
    }

    function caculation_products() {
        let products_total_price = 0;
        for (let i = 0; i < $('#mProducts #products_id').length - 1; i++) {
            let price = parseInt($('#mProducts #products_id option:selected').eq(i).data('price'));
            let quantity = parseInt($('#mProducts #products_quantity option:selected').eq(i).val());
            products_total_price += price * quantity;
        }
        $('#products_total_price').text(products_total_price);
    }

    init();
</script>

@endsection


