@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">庫存商品</a></li>
        <li class="breadcrumb-item active">編輯</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('products.update', $product->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">編輯庫存商品 : {{ $product->name }}</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">商品名稱*</th>
                    <td>
                        <input type="text" class="form-control" name="name" value="{{ $product->name }}">
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">商品類別*</th>
                    <td>
                        <select class="form-control" name="products_class_id">
                            <option value="">請選擇</option>
                            @foreach ($products_class as $row)
                                <option value="{{ $row->id }}" {{ $product->products_class_id == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-2">進貨廠商*</th>
                    <td>
                        <select class="form-control" name="vendors_id">
                            <option value="">請選擇</option>
                            @foreach ($vendors as $row)
                                <option value="{{ $row->id }}" {{ $product->vendors_id == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>型號*</th>
                    <td>
                        <input type="text" class="form-control" name="model" value="{{ $product->model }}">
                    </td>
                </tr>
                <tr>
                    <th>尺碼*</th>
                    <td>
                        <input type="text" class="form-control" name="size" value="{{ $product->size }}">
                    </td>
                </tr>
                <tr>
                    <th>顏色*</th>
                    <td>
                        <input type="text" class="form-control" name="color" value="{{ $product->color }}">
                    </td>
                </tr>
                <tr>
                    <th>單價*</th>
                    <td>
                        <input type="text" class="form-control" name="price" value="{{ $product->price }}">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo" value="{{ $product->memo }}">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Update</button>
            </p>
        </div>
    </div>
    </form>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">庫存數量 : {{ $product->stock }}</h3>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button class="btn btn-warning" data-toggle="modal" data-target="#mPurchase" >
                    進貨
                </button>

                <button class="btn btn-success" data-toggle="modal" data-target="#mShipment" >
                    出貨
                </button>

                <button class="btn btn-primary" data-toggle="modal" data-target="#mInventory" >
                    盤點
                </button>
            </p>
        </div>
    </div>

    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle" style="display:inline-block;">庫存紀錄</h3>
        </div>

        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-md-2">時間</th>
                        <th class="col-md-3">動作 / 對象</th>
                        <th class="col-md-1">進出數量</th>
                        <th class="col-md-1">在庫數量</th>
                        <th class="col-md-1">經手人員</th>
                        <th >備註</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product_logs as $row)
                    <tr>
                        <td>{{ $row->created_at }}</td>
                        <td>{!! $row->type_str !!}{{ empty($row->target_name)?'': " {$row->type_direction} {$row->target_name}"  }}</td>
                        <td>{{ $row->quantity }}</td>
                        <td>{{ $row->stock }}</td>
                        <td>{{ $row->User->name }}</td>
                        <td>{{ $row->memo }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            {!! $product_logs->appends(request()->except('page'))->links() !!}
        </div>
    </div>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>


<!-- Modal : 進貨 -->
<div id="mPurchase" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">進貨</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>請輸入進貨數量</th>
                        <td>
                            <input type="number" id="quantity" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button id="confirm" type="button" data-pid="{{ $product->id }}" class="btn btn-danger" onclick="Purchase(this)" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : 出貨 -->
<div id="mShipment" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">出貨</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>請輸入出貨數量</th>
                        <td>
                            <input type="number" id="quantity" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <th>出貨廠商對象</th>
                        <td>
                            <select class="form-control" id="vendors_id">
                                <option value="">請選擇</option>
                                @foreach ($vendors as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button id="confirm" type="button"  data-pid="{{ $product->id }}" class="btn btn-danger" onclick="Shipment(this)" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : 盤點 -->
<div id="mInventory" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">盤點</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>請輸入盤點後數量</th>
                        <td>
                            <input type="number" id="quantity" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button id="confirm" type="button"  data-pid="{{ $product->id }}" class="btn btn-danger" onclick="Inventory(this)" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Update Fail Message -->
<div id="mFail" class="modal modal-danger fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">更新失敗</h4>
            </div>
            <div class="modal-body">
                Error
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">關閉</button>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script>

    function init() {
    }

    function Purchase(element) {
        let pid = $(element).data('pid');

        $.post(`/products/${pid}/purchase`, {
            quantity: $('#mPurchase #quantity').val(),
            memo: $('#mPurchase #memo').val(),
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function Shipment(element) {
        let pid = $(element).data('pid');

        $.post(`/products/${pid}/shipment`, {
            quantity: $('#mShipment #quantity').val(),
            vendors_id: $('#mShipment #vendors_id').val(),
            memo: $('#mShipment #memo').val(),
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function Inventory(element) {
        let pid = $(element).data('pid');

        $.post(`/products/${pid}/inventory`, {
            quantity: $('#mInventory #quantity').val(),
            memo: $('#mInventory #memo').val(),
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    init();
</script>
@endsection

