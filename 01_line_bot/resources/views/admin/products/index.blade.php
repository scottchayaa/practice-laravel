<?php
    use Illuminate\Support\Facades\Redis;
    use Carbon\Carbon;
?>

@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">庫存商品</li>
        </ol>
    </nav>

    <div class="box">
        <form id="searchForm" method="GET">
        <div class="box-body">
            <div class="form-group row">
                <label for="products_class_id" class="control-label col-md-1">類別</label>
                <div class="col-md-2">
                    <select class="form-control" name="products_class_id">
                        <option value="">請選擇</option>
                        @foreach ($products_class as $row)
                            <option value="{{ $row->id }}" {{ request('products_class_id') == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
                        @endforeach
                    </select>
                </div>
                <label for="vendors_id" class="control-label col-md-1">進貨廠商</label>
                <div class="col-md-2">
                    <select class="form-control" name="vendors_id">
                        <option value="">請選擇</option>
                        @foreach ($vendors as $row)
                            <option value="{{ $row->id }}" {{ request('vendors_id') == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="model" class="control-label col-md-1">型號</label>
                <div class="col-md-2">
                    <input type="text" id="model" name="model" class="form-control" value="{{ request('model') }}" autofocus>
                </div>
                <label for="size" class="control-label col-md-1">尺碼</label>
                <div class="col-md-2">
                    <input type="text" id="size" name="size" class="form-control" value="{{ request('size') }}">
                </div>
                <label for="color" class="control-label col-md-1">顏色</label>
                <div class="col-md-2">
                    <input type="text" id="color" name="color" class="form-control" value="{{ request('color') }}">
                </div>
            </div>
        </div>
        </form>
        <div class="box-footer">
            <button id="search" class="btn btn-info"><i class="fa fa-search"></i> 搜尋</button>
            <button id="reset" class="btn btn-warning"><i class="fa fa-eraser"></i> 清除</button>
        </div>
    </div>

    @can('product_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('products.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: bold' }}">All</a></li> |
            <li><a href="{{ route('products.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: bold' : '' }}">Trash</a></li>
        </ul>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            @can('product_create')
            <a href="{{ route('products.create') }}" class="btn btn-default">
                <i class="fa fa-plus"></i> 新增
            </a>
            @endcan
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-md-1">類別</th>
                        <th class="col-md-2">商品名稱</th>
                        <th class="col-md-1">進貨廠商</th>
                        <th class="col-md-1">型號</th>
                        <th class="col-md-1">尺碼/顏色</th>
                        <th class="col-md-1">單價</th>
                        <th class="col-md-1">庫存數量</th>
                        <th class="col-md-1">最後進貨</th>
                        <th class="col-md-1">最後出貨</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $row)
                    <tr>
                        <td>{{ $row->class_name }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->vendor_name }}</td>
                        <td>{{ $row->model }}</td>
                        <td>{{ $row->size . ' / ' . $row->color }}</td>
                        <td>{{ $row->price }}</td>
                        <td>
                            <big>{{ $row->stock }}</big>
                            @if( request('show_deleted') != 1 )
                            <br>

                            <button class="btn btn-xs btn-warning" data-toggle="modal" data-target="#mPurchase" data-pid="{{ $row->id }}" onclick="passPid(this)">
                                進貨
                            </button>

                            <button class="btn btn-xs btn-success" data-toggle="modal" data-target="#mShipment" data-pid="{{ $row->id }}" onclick="passPid(this)">
                                出貨
                            </button>

                            <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#mInventory" data-pid="{{ $row->id }}" onclick="passPid(this)">
                                盤點
                            </button>

                            @endif
                        </td>
                        <td>
                            @if (Redis::hget("products:{$row->id}", "last_purchase_at"))
                            {{ Redis::hget("products:{$row->id}", "last_purchase_user") }}
                            <br>
                            <small style="color:#999;">
                                {{ Carbon::parse(Redis::hget("products:{$row->id}", "last_purchase_at"))->diffForHumans() }}
                            </small>
                            @endif
                        </td>
                        <td>
                            @if (Redis::hget("products:{$row->id}", "last_shipment_at"))
                            {{ Redis::hget("products:{$row->id}", "last_shipment_user") }}
                            <br>
                            <small style="color:#999;">
                                {{ Carbon::parse(Redis::hget("products:{$row->id}", "last_shipment_at"))->diffForHumans() }}
                            </small>
                            @endif
                        </td>
                        <td>
                            {!! view($template, compact('row', 'gateKey', 'routeKey')) !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

<!-- Modal : 進貨 -->
<div id="mPurchase" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">進貨</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>請輸入進貨數量</th>
                        <td>
                            <input type="number" id="quantity" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button id="confirm" type="button" data-pid="-1" class="btn btn-danger" onclick="Purchase(this)" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : 出貨 -->
<div id="mShipment" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">出貨</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>請輸入出貨數量</th>
                        <td>
                            <input type="number" id="quantity" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <th>出貨廠商對象</th>
                        <td>
                            <select class="form-control" id="vendors_id">
                                <option value="">請選擇</option>
                                @foreach ($vendors as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button id="confirm" type="button"  data-pid="-1" class="btn btn-danger" onclick="Shipment(this)" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : 盤點 -->
<div id="mInventory" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">盤點</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr>
                        <th>請輸入盤點後數量</th>
                        <td>
                            <input type="number" id="quantity" class="form-control" >
                        </td>
                    </tr>
                    <tr>
                        <th>備註</th>
                        <td>
                            <input id="memo" class="form-control" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button id="confirm" type="button"  data-pid="-1" class="btn btn-danger" onclick="Inventory(this)" data-dismiss="modal">確認</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Modal : Update Fail Message -->
<div id="mFail" class="modal modal-danger fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">更新失敗</h4>
            </div>
            <div class="modal-body">
                Error
            </div>
            <div class="modal-footer">
                <p class="text-center">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">關閉</button>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>

    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });
    }

    function passPid(element) {
        let modal = $(element).data('target');
        let pid = $(element).data('pid');
        $(modal + ' #confirm').attr('data-pid', pid);
    }

    function Purchase(element) {
        let pid = $(element).data('pid');
        let apiUrl = '{{ route("products.purchase", '@') }}';

        $.post(apiUrl.replace('@', pid), {
            quantity: $('#mPurchase #quantity').val(),
            memo: $('#mPurchase #memo').val(),
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function Shipment(element) {
        let pid = $(element).data('pid');
        let apiUrl = '{{ route("products.shipment", '@') }}';

        $.post(apiUrl.replace('@', pid), {
            quantity: $('#mShipment #quantity').val(),
            vendors_id: $('#mShipment #vendors_id').val(),
            memo: $('#mShipment #memo').val(),
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    function Inventory(element) {
        let pid = $(element).data('pid');
        let apiUrl = '{{ route("products.inventory", '@') }}';

        $.post(apiUrl.replace('@', pid), {
            quantity: $('#mInventory #quantity').val(),
            memo: $('#mInventory #memo').val(),
        }).done(function (res){
            console.log('成功');
            console.log(res);
            location.reload();
        }).fail(function (error){
            console.log(error.status + ' 失敗');
            console.log(error.responseJSON);
            $('#mFail').modal('show');
            $('#mFail .modal-body').html(error.responseJSON.message);
        });
    }

    init();
</script>
@endsection
