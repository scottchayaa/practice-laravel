@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">庫存商品</a></li>
        <li class="breadcrumb-item active">{{ $product->name }}</li>
        </ol>
    </nav>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">庫存商品</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">商品名稱</th>
                    <td>{{ $product->name }}</td>
                </tr>
                <tr>
                    <th>型號</th>
                    <td>{{ $product->model }}</td>
                </tr>
                <tr>
                    <th>尺碼</th>
                    <td>{{ $product->size }}</td>
                </tr>
                <tr>
                    <th>顏色</th>
                    <td>{{ $product->color }}</td>
                </tr>
                <tr>
                    <th>單價</th>
                    <td>{{ $product->price }}</td>
                </tr>
                <tr>
                    <th>庫存數量</th>
                    <td>{{ $product->stock }}</td>
                </tr>
            </table>
        </div>
    </div>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


