@extends('layouts.app')

@section('style')
<link href="{{ url('/css/MonthPicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">班別冊製作</li>
        </ol>
    </nav>

    <div class="box">
        <form id="searchForm" method="GET">
        <div class="box-body">
            <div class="form-group row">
                <label for="courses_id" class="control-label col-md-1">班別：</label>
                <div class="col-md-2">
                    <select class="form-control" id="courses_id" name="courses_id">
                        <option value="">請選擇</option>
                        @foreach ($courses as $row)
                            <option value="{{ $row->id }}" {{ request('courses_id') == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
                        @endforeach
                    </select>
                </div>
                <label for="Ym" class="control-label col-md-1">搜尋月份：</label>
                <div class="col-md-2">
                    <input type="text" id="Ym" name="Ym" class="form-control" value="{{ request('Ym') }}" autocomplete="off">
                </div>
            </div>
        </div>
        </form>
        <div class="box-footer">
            <button id="search" class="btn btn-info"><i class="fa fa-search"></i> 搜尋</button>
            <button id="reset" class="btn btn-warning"><i class="fa fa-eraser"></i> 清除</button>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <a href="{{ route('report.courses_members.export') . '?' . request()->getQueryString() }}" target="_blank" class="btn btn-default">
                <i class="fa fa-file-excel-o"></i> 匯出Excel
            </a>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover" >
                <thead>
                    <tr>
                        <th class="col-md-1">繳費編號</th>
                        <th class="col-md-2">班別</th>
                        <th class="col-md-1">非會員編號</th>
                        <th class="col-md-1">會員編號</th>
                        <th class="col-md-2">姓名</th>
                        <th class="col-md-1">級數</th>
                        <th >報名月份</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses_members as $row)
                    <tr>
                        <td>{{ $row->order_id }}</td>
                        <td>{{ $row->course_name }}</td>
                        <td>{{ $row->no_card_id }}</td>
                        <td>{{ $row->card_id }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->courses_now_level }}</td>
                        <td>{{ $row->courses_month_select }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
<script src="{{ url('/js/MonthPicker.min.js') }}"></script>
<script>
    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });

        $('#Ym').MonthPicker({
            Button: false,
            MonthFormat: 'yy-mm',
            i18n: {
                year: '年',
                jumpYears: "選擇年分",
                backTo: "返回",
                months: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
            }
        });
    }

    init();
</script>
@endsection
