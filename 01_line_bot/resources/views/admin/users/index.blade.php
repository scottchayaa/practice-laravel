@extends('layouts.app')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item ">系統管理</li>
            <li class="breadcrumb-item active">帳號管理</li>
        </ol>
    </nav>

    @can('user_create')
    <p>
        <a href="/admin/users/create" class="btn btn-success">新增</a>
    </p>
    @endcan

    <div class="box">
        <div class="box-header">
            {{-- hearder... --}}
        </div>

        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($users) > 0)
                        @foreach ($users as $user)
                            <tr>
                                <td field-key='name'>{{ $user->name }}</td>
                                <td field-key='email'>{{ $user->email }}</td>
                                <td field-key='role'>{{ $user->role->title ?? '' }}</td>
                                <td>
                                    @can('user_edit')
                                    <a href="/admin/users/{{ $user->id }}" class="btn btn-xs btn-info">edit</a>
                                    @endcan
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">No Data</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

