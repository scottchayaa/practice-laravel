@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('vendors.index') }}">廠商管理</a></li>
        <li class="breadcrumb-item active">編輯</li>
        </ol>
    </nav>

    <form method="POST" action="{{ route('vendors.update', $vendor->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="text-primary showTitle">編輯廠商資料</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">公司名稱</th>
                    <td>
                        <input type="text" class="form-control" name="name" value="{{ $vendor->name }}">
                    </td>
                </tr>
                <tr>
                    <th>統一編號</th>
                    <td>
                        <input type="text" class="form-control" name="tax_id" value="{{ $vendor->tax_id }}">
                    </td>
                </tr>
                <tr>
                    <th>公司電話</th>
                    <td>
                        <input type="text" class="form-control" name="telephone" value="{{ $vendor->telephone }}">
                    </td>
                </tr>
                <tr>
                    <th>公司傳真</th>
                    <td>
                        <input type="text" class="form-control" name="fax_number" value="{{ $vendor->fax_number }}">
                    </td>
                </tr>
                <tr>
                    <th>公司地址</th>
                    <td>
                        <input type="text" class="form-control" name="address" value="{{ $vendor->address }}">
                    </td>
                </tr>
                <tr>
                    <th>負責人</th>
                    <td>
                        <input type="text" class="form-control" name="contact_person" value="{{ $vendor->contact_person }}">
                    </td>
                </tr>
                <tr>
                    <th>負責人電話</th>
                    <td>
                        <input type="text" class="form-control" name="contact_telephone" value="{{ $vendor->contact_telephone }}">
                    </td>
                </tr>
                <tr>
                    <th>負責人手機</th>
                    <td>
                        <input type="text" class="form-control" name="contact_cellphone" value="{{ $vendor->contact_cellphone }}">
                    </td>
                </tr>
                <tr>
                    <th>負責人Email</th>
                    <td>
                        <input type="text" class="form-control" name="contact_email" value="{{ $vendor->contact_email }}">
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td>
                        <input type="text" class="form-control" name="memo" value="{{ $vendor->memo }}">
                    </td>
                </tr>
            </table>
        </div>
        <div class="box-footer">
            <p class="text-center">
                <button type="submit" class="btn btn-danger">Update</button>
            </p>
        </div>
    </div>
    </form>

    <a href="javascript:history.back()" class="btn btn-default">返 回</a>

    <br>
    <br>
    <br>
@endsection


