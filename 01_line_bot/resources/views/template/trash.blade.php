@can($gateKey.'_delete')
    <form method="POST" action="{{ "/admin/{$routeKey}/restore/{$row->id}" }}" style="display: inline-block;" onsubmit="return confirm('確認復原?');">
        {{ csrf_field() }}
        <input class="btn btn-xs btn-success" type="submit" value="復原">
    </form>
@endcan
{{-- @can($gateKey.'delete')
    {!! Form::open(array(
        'style' => 'display: inline-block;',
        'method' => 'DELETE',
        'onsubmit' => "return confirm('確認永久刪除?');",
        'route' => [$routeKey.'.perma_del', $row->id])) !!}
    {!! Form::submit('永久刪除', array('class' => 'btn btn-xs btn-danger')) !!}
    {!! Form::close() !!}
@endcan --}}
