<?php

// Web (Frontend)
Route::group(['namespace' => 'Web'], function () {
    Route::get('/member/line/login', 'Auth\LoginController@lineLogin');

    Route::get('/demo', 'Member\MemberController@index')->name('demo');
});
