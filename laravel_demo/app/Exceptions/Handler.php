<?php

namespace App\Exceptions;

use Exception;

use App\Exceptions\HttpException;

use App\Http\Responses\ApiErrorResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception                $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (!$e instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
            if ($e instanceof ModelNotFoundException) {
                throw new HttpException(404, 0, $e);
            }

            if ($e instanceof AuthorizationException) {
                throw new HttpException(403, 0, $e);
            }

            if ($e instanceof ValidationException) {
                throw new HttpException(422, 0, $e, $e->errors());
            }

            throw new HttpException(500, 999, $e);
        }

        if ($e instanceof NotFoundHttpException) {
            throw new HttpException(404, 0, $e);
        }

        if ($e instanceof MethodNotAllowedHttpException) {
            throw new HttpException(405, 0, $e, null, explode(',', $e->getHeaders()['Allow']));
        }

        return new ApiErrorResponse($e);

        // Origin code...
        //return parent::render($request, $e);
    }
}
