<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\Articles;
use App\Libs\Facades\Common;
use Illuminate\Http\Request;
use App\Services\ArticlesService;
use App\Http\Responses\ApiResponse;
use App\Repositories\ArticlesRepository;
use App\Http\Requests\Articles\IndexRequest;
use App\Http\Requests\Articles\CreateRequest;
use App\Http\Requests\Articles\UpdateRequest;

class ArticlesController extends Controller
{
    private $articlesRepo;

    public function __construct(ArticlesRepository $articlesRepo)
    {
        $this->articlesRepo = $articlesRepo;
    }

    public function index(IndexRequest $request)
    {
        return new ApiResponse($this->articlesRepo->getPaginate($request->all()), 200);
    }

    public function create(CreateRequest $request)
    {
        $newArticle = $this->articlesRepo->create($request->all());

        return new ApiResponse($newArticle, 201);
    }

    public function update(UpdateRequest $request, int $articleId)
    {
        $success = $this->articlesRepo->update($articleId, $request->all());

        return new ApiResponse(["success" => $success], 201);
    }

    public function delete(int $articleId)
    {
        $success = $this->articlesRepo->delete($articleId);

        return new ApiResponse(["success" => $success], 200);
    }
}
