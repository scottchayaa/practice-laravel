<?php

namespace App\Http\Controllers;

use Common;
use DB;
use App\Models\Stocks;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpKernel\Exception\HttpException;

class StockController extends Controller
{
    protected $productId = 1;

    // 有高併發問題
    // 如果同時多處理程序執行
    // 則會出現資料扣除數量不一致問題
    // 例如同時20筆request近來, 結果實際扣除數量不是20
    public function minus()
    {
        $product = Stocks::find($this->productId);
        $number = $product->number;

        if ($number > 0) {
            $number--;
            $product->number = $number;
            $product->save();
        } else {
            return Common::jsonResponse(
                [
                    "error" => "stock not enough !"
                ],
                400
            );
        }

        return Common::jsonResponse(
            [
                "stock" => $number
            ],
            200
        );
    }

    //使用transaction
    public function minus2()
    {
        // 1.開始交易
        DB::beginTransaction();
        try {
            // 2.查詢時進行鎖表
            $product = Stocks::lockForUpdate()->find($this->productId);

            // 3.鎖表後進行參數相關處理
            $number =  $product->number;
            if ($number <= 0) {
                throw new HttpException(403, "stock not enough !", null, [], 0);
            }
            $number--;

            // 4.更新資料(尚未提交)
            $product->number = $number;
            $product->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw new HttpException(500, $e->getMessage(), $e, [], 0);
        }

        // 5.提交結果
        DB::commit();

        return Common::jsonResponse(
            [
                "stock" => $product->number
            ],
            200
        );
    }

    //
    public function minus2_2()
    {

        DB::beginTransaction();
        try {
            // 先扣庫存, 若小於0則復原
            Stocks::where('id', $this->productId)->decrement('number');
            $number = Stocks::lockForUpdate()->find($this->productId)->number;
            if ($number < 0) {
                DB::rollback();
                throw new HttpException(403, "stock not enough !", null, [], 0);
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw new HttpException(500, $e->getMessage(), $e, [], 0);
        }

        DB::commit();

        return Common::jsonResponse(
            [
                "stock" => $number
            ],
            200
        );
    }

    // 一樣有minus的問題
    public function minus3()
    {
        $pKey = "product" . $this->productId;
        $number = Redis::get($pKey);

        if ($number > 0) {
            $number--;
            Redis::set($pKey, $number);
        } else {
            return Common::jsonResponse(
                [
                    "error" => "stock not enough !"
                ],
                400
            );
        }

        return Common::jsonResponse(
            [
                "stock" => $number
            ],
            200
        );
    }

    public function minus4()
    {
        $pKey = "product" . $this->productId;

        //嘗試獲取鎖
        $lock = Redis::setnx("lock:$pKey", 1);

        //如果獲取不到鎖
        while ($lock == 0) {
            try {
                usleep(100000); // 100ms
                //嘗試再次獲取
                $lock = Redis::setnx("lock:$pKey", 1);
            } catch (\Exception $e) {
                throw new HttpException(500, "error get lock key", $e, [], 0);
            }
        }
        //如果獲取鎖, 則設置3秒過期時間, 以防鎖死
        Redis::expire("lock:$pKey", 3000);

        $number = Redis::get($pKey);

        if ($number > 0) {
            $number--;
            Redis::set($pKey, $number);
            Redis::del("lock:$pKey");

            return Common::jsonResponse(
                [
                    "stock" => $number
                ],
                200
            );
        } else {
            Redis::del("lock:$pKey");
            return Common::jsonResponse(
                [
                    "error" => "stock not enough !"
                ],
                400
            );
        }
    }
}
