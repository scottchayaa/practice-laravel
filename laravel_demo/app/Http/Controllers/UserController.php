<?php

namespace App\Http\Controllers;

use Mail;
use Common;
use JWTAuth;

use Exception;
use App\Models\Users;
use App\Mail\SignupActivate;
use Illuminate\Http\Request;

use App\Exceptions\HttpException;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserController extends Controller
{
    /**
     * 回傳授權碼資訊
     * access_token, token_type, expires_in
     *
     * @param string $token 授權碼
     *
     * @return \Illuminate\Http\JsonResponse  The info of auth token
     */
    protected function respondWithToken($token)
    {
        return Common::jsonResponse(
            [
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => JWTAuth::factory()->getTTL() * 60
            ],
            200
        );
    }

    public function emailSignup(Request $request)
    {
        Common::validateRequest(
            $request,
            [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            ]
        );

        $activation_token = str_random(60);
        $user_data = json_encode(
            [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activation_token' => $activation_token
            ]
        );

        Common::setRedisTokenRetry("signup:" . $request->email, $user_data, config('services.email.ttl'), config('services.email.retry_after'));

        Mail::to($request->email)->send(
            new SignupActivate(
                url('/api/users/activate/' . $request->email . '/' . $activation_token)
            )
        );

        return Common::jsonResponse(
            [
            'message' => 'Successfully send activate email to ' . $request->email,
            ],
            201
        );
    }

    /**
     * 帳號認證
     * 驗證是否收到合法email認證信
     *
     * @param string $email
     * @param string $token
     *
     * @return void
     */
    public function emailActivate(string $email, string $token)
    {
        $value = Redis::get("signup:" . $email);
        if (!$value) {
            throw new HttpException(400, 101, new Exception('no match data in redis.'));
        }
        $value = json_decode($value);
        if ($value->activation_token != $token) {
            throw new HttpException(400, 101, new Exception('activation_token is not correct.'));
        }

        Users::create(
            [
            "name" => $value->name,
            "email" => $value->email,
            "password" => $value->password,
            ]
        );
        Common::delRedisTokenRetry("signup:" . $email);

        return Common::jsonResponse(
            [
            'message' => 'Successfully activate account : ' . $value->email,
            ],
            200
        );
    }

    public function login(Request $request)
    {
        Common::validateRequest(
            $request,
            [
            'email' => 'required|email',
            'password' => 'required',
            ]
        );

        $credentials = $request->only('email', 'password');

        if (Redis::exists('signup:' . $request->email)) {
            throw new HttpException(401, 102);
        }

        if (!$token = JWTAuth::attempt($credentials)) {
            throw new HttpException(401, 103);
        }

        return $this->respondWithToken($token);
    }

    public function logout(Request $request)
    {
        $token = $request->header('Authorization');

        JWTAuth::invalidate($token);
        return Common::jsonResponse(
            [
            'message' => 'Successfully logged out',
            ],
            200
        );
    }
}
