<?php

namespace App\Http\Requests\Articles;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'size' => ['integer'],
            'page' => ['integer'],
            'sort' => ['string'],
            'login' => ['boolean'],
            'q' => ['string'],
        ];
    }

    public function attributes()
    {
        return [
            'size' => 'size',
            'page' => 'page',
            'sort' => 'sort',
            'login' => 'login',
            'q' => 'q',
        ];
    }

    public function messages()
    {
        return [
            'integer' => ':attribute 必須為數字',
            'string' => ':attribute 必須為字串',
            'boolean' => ':attribute 必須為true或false',
        ];
    }
}
