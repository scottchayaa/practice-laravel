<?php

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiErrorResponse extends JsonResponse
{
    public function __construct(HttpException $e)
    {
        $status = $e->getStatusCode();
        $headers = $e->getHeaders();
        $data = [
            "code" => $e->getCode(),
            "msg"  => $e->getMessage(),
        ];

        if (method_exists($e, 'getData')) {
            if ($e->getData()) {
                $data["data"] = $e->getData();
            }
        }

        if (config("app.debug")) {
            $data["debug"] = [];
            do {
                array_push(
                    $data["debug"],
                    [
                    "msg" => $e->getMessage(),
                    "class" => get_class($e),
                    "file" => $e->getFile(),
                    "line" => $e->getLine()
                    ]
                );
            } while ($e = $e->getPrevious());
        }

        parent::__construct($data, $status, $headers, JSON_UNESCAPED_UNICODE);
    }
}
