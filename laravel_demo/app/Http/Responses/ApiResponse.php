<?php

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class ApiResponse extends JsonResponse
{
    public function __construct($data, int $status = 200, array $headers = [])
    {
        if (!is_array($data)) {
            $data = $data->toArray();
        }

        parent::__construct($data, $status, $headers, JSON_UNESCAPED_UNICODE);
    }
}
