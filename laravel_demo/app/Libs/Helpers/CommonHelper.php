<?php

namespace App\Libs\Helpers;

class CommonHelper
{
    /**
     * 將$_SERVER封裝
     *
     * @param string $arg 
     * 
     * @return string
     */
    public function SERVER(string $arg) : string
    {
        try {
            return $_SERVER[$arg];
        } catch (\Exception $e) {
            return "";
        }
    }

    /**
     * 將config()封裝
     *
     * @param string $arg 
     * 
     * @return string
     */
    public function config(string $arg) : string
    {
        return (config($arg) === null) ? "" : config($arg);
    }
}