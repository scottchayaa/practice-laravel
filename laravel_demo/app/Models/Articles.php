<?php

namespace App\Models;

use JWTAuth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Articles extends Model
{
    protected $table = "articles";
    protected $primaryKey = 'id';

    protected $fillable = ['users_id', 'title', 'content'];

    /**
     * Cutomized get method
     *
     * @param Builder $query
     * @param bool    $login
     *
     * @return Model
     */
    public function scopeIGet(Builder $query, bool $login = false)
    {
        $result;
        if ($login) {
            try {
                if (! $user = JWTAuth::parseToken()->authenticate()) {
                    throw new UnauthorizedHttpException('jwt-auth', 'User not found');
                }
            } catch (JWTException $e) {
                throw new UnauthorizedHttpException('jwt-auth', $e->getMessage(), $e, $e->getCode());
            }
            $result = $query->where('users_id', $user->id);
        }
        $result = $query->get();

        return $result;
    }
}
