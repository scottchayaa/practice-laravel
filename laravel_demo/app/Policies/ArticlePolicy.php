<?php

namespace App\Policies;

use App\Models\Users;
use App\Models\Articles;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(Users $user, Articles $article)
    {
        return $user->id === $article->users_id;
    }

    public function delete(Users $user, Articles $article)
    {
        return $user->id === $article->users_id;
    }
}
