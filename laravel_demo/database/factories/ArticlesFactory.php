<?php

use Faker\Generator as Faker;

$factory->define(
    App\Models\Articles::class, function (Faker $faker) {
        //$ary_users_id = App\Models\Users::all()->pluck('id');
        return [
        'users_id' => function () {
            return App\Models\Users::inRandomOrder()->first()->id;
        },
        'title' => $faker->sentence(6),
        'content' => $faker->text(200),
        ];
    }
);
