<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{

    public $set_schema_table = 'articles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            $this->set_schema_table, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('users_id');
                $table->string('title');
                $table->text('content');
                $table->timestamps();

                $table->foreign('users_id')
                    ->references('id')->on('users')
                    ->onDelete('no action')
                    ->onUpdate('no action');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
