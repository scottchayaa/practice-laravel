@component('mail::message')
# Hello !

Thanks for signup! Please before you begin, you must confirm your account.

@component('mail::button', ['url' => $url])
啟用帳號
@endcomponent

Thank you for using our application!

Regards,<br>
{{ config('app.name') }}
@endcomponent
