<?php

use App\Services\Hsinchu;
use App\Services\BlackCat;
use App\Services\PostOffice;
use Illuminate\Http\Request;
use App\Services\ShippingService;
use App\Services\LogisticsFactory;

Route::post('/users/signup', 'UserController@emailSignup');
Route::get('/users/activate/{email}/{token}', 'UserController@emailActivate');
Route::post('/users/login', 'UserController@login');
Route::get('/users/logout', 'UserController@logout')->middleware('jwt.auth');

Route::get('/articles', 'ArticlesController@index');
Route::post('/articles', 'ArticlesController@create')->middleware(['jwt.auth']);
Route::put('/articles/{articleId}', 'ArticlesController@update')->middleware(['jwt.auth']);
Route::delete('/articles/{articleId}', 'ArticlesController@delete')->middleware(['jwt.auth']);

// Demo : 庫存高併發問題
Route::get('/stock/minus', 'StockController@minus');
Route::get('/stock/minus2', 'StockController@minus2');
Route::get('/stock/minus2-2', 'StockController@minus2_2');
Route::get('/stock/minus3', 'StockController@minus3');
Route::get('/stock/minus4', 'StockController@minus4');

// LINE
Route::post('/line/webhook', 'LineController@webhook');
Route::post('/line/push', 'LineController@push');


// 工廠模式
Route::get('/demo', function () {

    $weight = 60;

    //原先
    // $service = new ShippingService();
    // $result = [
    //     "BlackCat" => $service->calculateFee('BlackCat', $weight),
    //     "Hsinchu" => $service->calculateFee('Hsinchu', $weight),
    //     "PostOffice" => $service->calculateFee('PostOffice', $weight),
    // ];

    //後來
    // $result = [
    //     "BlackCat" => (new ShippingService(new BlackCat))->calculateFee($weight),
    //     "Hsinchu" =>  (new ShippingService(new Hsinchu))->calculateFee($weight),
    //     "PostOffice" =>  (new ShippingService(new PostOffice))->calculateFee($weight),
    // ];

    $result = [
        "BlackCat" => (new ShippingService(LogisticsFactory::create('BlackCat')))->calculateFee($weight),
        "Hsinchu" =>  (new ShippingService(LogisticsFactory::create('Hsinchu')))->calculateFee($weight),
        "PostOffice" =>  (new ShippingService(LogisticsFactory::create('PostOffice')))->calculateFee($weight),
    ];

    return response()->json($result);
});
