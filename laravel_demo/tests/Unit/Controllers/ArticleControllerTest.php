<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;
use App\Models\Articles;
use App\Http\Responses\ApiResponse;
use App\Repositories\ArticlesRepository;
use App\Http\Controllers\ArticlesController;
use App\Http\Requests\Articles\IndexRequest;
use App\Http\Requests\Articles\CreateRequest;
use App\Http\Requests\Articles\UpdateRequest;

class ArticleControllerTest extends TestCase
{
    /** @test */
    public function index()
    {
        // arrange
        $expected = [
            ['name' => 'example', 'email' => 'example@gmail.com']
        ];

        $request = new IndexRequest;

        $mock = $this->initMock(ArticlesRepository::class);
        $mock->shouldReceive('getList')->once()->withAnyArgs()->andReturn($expected);

        $target = app(ArticlesController::class);

        // act
        $apiResponse = $target->index($request);

        // assert
        $this->assertEquals(new ApiResponse($expected, 200), $apiResponse);
    }

    public function testCreate()
    {
        // arrange
        $expected = [];
        $request = new CreateRequest();
        $request->replace($expected);

        $mock = $this->initMock(ArticlesRepository::class);
        $mock->shouldReceive('create')
            ->once()
            ->with($expected)
            ->andReturn($expected);

        $target = app(ArticlesController::class);

        // act
        $apiResponse = $target->create($request);

        // assert
        $this->assertEquals(new ApiResponse($expected, 201), $apiResponse);
    }

    public function testUpdate()
    {
        // arrange
        $input1 = 1;
        $input2 = [];
        $expected = true;
        $request = new UpdateRequest();
        $request->replace($input2);

        $mock = $this->initMock(ArticlesRepository::class);
        $mock->shouldReceive('update')
            ->once()
            ->with($input1, $input2)
            ->andReturn($expected);

        $target = app(ArticlesController::class);

        // act
        $apiResponse = $target->update($request, $input1);

        // assert
        $this->assertEquals(new ApiResponse(["success" => $expected], 201), $apiResponse);
    }

    public function testDelete()
    {
        // arrange
        $input1 = 1;
        $expected = true;

        $mock = $this->initMock(ArticlesRepository::class);
        $mock->shouldReceive('delete')
            ->once()
            ->with($input1)
            ->andReturn($expected);

        $target = app(ArticlesController::class);

        // act
        $apiResponse = $target->delete($input1);

        // assert
        $this->assertEquals(new ApiResponse(["success" => $expected], 200), $apiResponse);
    }
}
