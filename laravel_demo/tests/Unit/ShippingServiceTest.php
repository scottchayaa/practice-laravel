<?php

use Tests\TestCase;
use App\Services\Hsinchu;
use App\Services\BlackCat;
use App\Services\ShippingService;
use App\Services\LogisticsInterface;

class ShippingServiceTest extends TestCase
{
    /** @test */
    public function 黑貓單元測試()
    {
        /** arrange */
        $mock = Mockery::mock(BlackCat::class);
        $mock->shouldReceive('calculateFee')
            ->once()
            ->withAnyArgs()
            ->andReturn(110);

        App::instance(LogisticsInterface::class, $mock, Hsinchu::class);

        /** act */
        $weight = 1;
        $actual = App::make(ShippingService::class)->calculateFee($weight);

        /** assert */
        $expected = 110;
        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function 黑貓整合測試()
    {
        /** arrange */
        $this->app->bind(LogisticsInterface::class, BlackCat::class);

        /** act */
        $weight = 3;
        $actual = App::make(ShippingService::class)->calculateFee($weight);

        /** assert */
        $expected = 130;
        $this->assertEquals($expected, $actual);
    }
}